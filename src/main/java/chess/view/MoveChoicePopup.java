package chess.view;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.awt.Component;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.util.concurrent.Executor;
import chess.controller.MoveChoiceActionListener;
import chess.model.BoardModel;
import chess.model.GameModel;
import chess.model.MoveModel;
/*
 * Created by andreasm on 03.07.2014.
 */
public class MoveChoicePopup extends PopupMenu
{
    private final BoardModel boardModel;
    private final GameModel gameModel;
    private final MoveModel moveModel;
    private final Executor executor;
    //
    public MoveChoicePopup(Executor executor, BoardModel boardModel, GameModel gameModel, MoveModel moveModel)
    {
        super();
        //
        this.executor = executor;
        this.boardModel = boardModel;
        this.gameModel = gameModel;
        this.moveModel = moveModel;
    }
    //
    @Override
    public void show(Component origin, int x, int y)
    {
        removeAll();
        setItems();
        super.show(origin, x, y);
    }
    //
    private void setItems()
    {
        var moves = moveModel.getMoves();
        //
        for (var move : moves)
        {
            var label = move.toString();
            var listener = new MoveChoiceActionListener(boardModel, gameModel, move, executor);
            var item = new MenuItem(label);
            item.addActionListener(listener);
            add(item);
        }
    }
}