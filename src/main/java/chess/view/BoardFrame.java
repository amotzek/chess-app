package chess.view;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Image;
import java.util.Arrays;
import java.util.List;
import chess.board.Color;
import chess.model.GameModelListener;
import chess.move.Move;
import chess.piece.Piece;
import javax.swing.ImageIcon;
/*
 * Created by andreasm on 27.05.2014.
 */
public final class BoardFrame extends Frame implements GameModelListener
{
    private String player;
    private String message;
    private String info;
    //
    public BoardFrame(BoardCanvas boardCanvas)
    {
        super("Schach");
        //
        player = "Weiß";
        message = null;
        info = null;
        //
        add(boardCanvas);
        setResizable(false);
        setUndecorated(true);
        setIconImages(createIconImages());
        pack();
    }
    //
    public void playerChanged(Color playerColor, Color humanColor)
    {
        if (playerColor == Color.WHITE)
        {
            player = "Weiß";
            changeTitle();
        }
        else
        {
            player = "Schwarz";
            changeTitle();
        }
        //
        if (playerColor == humanColor)
        {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        else
        {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
    }
    //
    public void pieceCleared()
    {
        message = null;
        changeTitle();
    }
    //
    public void pieceTaken(Piece piece)
    {
        message = piece.toString();
        info = null;
        changeTitle();
    }
    //
    public void piecePut(Move move)
    {
        message = move.toString();
        changeTitle();
    }
    //
    public void info(String info)
    {
        this.info = info;
    }
    //
    private void changeTitle()
    {
        var builder = new StringBuilder();
        builder.append("Schach - ");
        builder.append(player);
        builder.append(" am Zug");
        //
        if (message != null)
        {
            builder.append(" - ");
            builder.append(message);
        }
        //
        if (info != null)
        {
            builder.append(" - ");
            builder.append(info);
        }
        //
        setTitle(builder.toString());
    }
    //
    private List<Image> createIconImages()
    {
        return Arrays.asList(createIconImage("ICON_APP16.png"), createIconImage("ICON_APP48.png"));
    }
    //
    private Image createIconImage(String name)
    {
        var url = getClass().getResource(name);
        var icon = new ImageIcon(url);
        //
        return icon.getImage();
    }
}