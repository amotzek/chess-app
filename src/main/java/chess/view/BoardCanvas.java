package chess.view;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Polygon;
import java.awt.image.ImageProducer;
import java.io.IOException;
import java.util.HashMap;
import java.util.function.Function;
import chess.board.Color;
import chess.board.Position;
import chess.model.BoardModel;
import chess.model.BoardModelListener;
import chess.model.GameModel;
import chess.model.GameModelListener;
import chess.move.Move;
import chess.piece.Piece;
/*
 * Created by andreasm on 27.05.2014.
 */
public final class BoardCanvas extends Canvas implements GameModelListener, BoardModelListener
{
    private static final java.awt.Color FRAME_COLOR = new java.awt.Color(0xa0a0ff);
    private static final java.awt.Color BLACK_SQUARE_COLOR = new java.awt.Color(0xd0d0ff);
    private static final java.awt.Color WHITE_SQUARE_COLOR = java.awt.Color.WHITE;
    private static final java.awt.Color DARK_MARK_COLOR = new java.awt.Color(0xcfffc0);
    private static final java.awt.Color LIGHT_MARK_COLOR = new java.awt.Color(0xefffe0);
    private static final java.awt.Color DARK_ATTACKED_COLOR = new java.awt.Color(0xffa0a0);
    private static final java.awt.Color LIGHT_ATTACKED_COLOR = new java.awt.Color(0xffe0e0);
    private static final int BORDER = 23;
    private static final int SQUARE_SIDE = 46;
    private static final int BOARD_SIDE = 8 * SQUARE_SIDE;
    private static final int CANVAS_SIDE = BOARD_SIDE + 2 * BORDER;
    public static final int CROSS_X = 8 * SQUARE_SIDE + BORDER;
    public static final int CROSS_Y = 1;
    public static final int CROSS_SIDE = 20;
    private static final Polygon CROSS = createCross(CROSS_X, CROSS_Y);
    public static final int ARROW_X = 1;
    public static final int ARROW_Y = 8 * SQUARE_SIDE + BORDER;
    public static final int ARROW_SIDE = 20;
    private static final Polygon ARROW = createArrow(ARROW_X, ARROW_Y);
    //
    private final GameModel gameModel;
    private final BoardModel boardModel;
    private final HashMap<String, Image> imageByName;
    private volatile Image contentImage;
    //
    public BoardCanvas(GameModel gameModel, BoardModel boardModel)
    {
        this.gameModel = gameModel;
        this.boardModel = boardModel;
        imageByName = new HashMap<>();
    }
    //
    public void playerChanged(Color playerColor, Color humanColor)
    {
        changeContent();
    }
    //
    public void pieceCleared()
    {
        changeContent();
    }
    //
    public void pieceTaken(Piece piece)
    {
        changeContent();
    }
    //
    public void piecePut(Move move)
    {
    }
    //
    public void info(String message)
    {
    }
    //
    public void boardModelChanged()
    {
        changeContent();
    }
    //
    public Position coordinatesToPosition(int x, int y)
    {
        x -= BORDER;
        y -= BORDER;
        x /= SQUARE_SIDE;
        y /= SQUARE_SIDE;
        //
        if (gameModel.playsHumanWhite())
        {
            y = 7 - y;
        }
        else
        {
            x = 7 - x;
        }
        //
        try
        {
            return Position.getInstance(x, y);
        }
        catch (IllegalArgumentException e)
        {
            return null;
        }
    }
    //
    private void changeContent()
    {
        var image = contentImage;
        //
        if (image != null) image.flush();
        //
        contentImage = null;
        repaint(300L);
    }
    //
    @Override
    public void update(Graphics graphics)
    {
        paint(graphics);
    }
    //
    @Override
    public void paint(Graphics graphics)
    {
        var contentImage = this.contentImage;
        //
        if (contentImage == null)
        {
            contentImage = createImage(CANVAS_SIDE, CANVAS_SIDE);
            var imageGraphics = contentImage.getGraphics();
            paintBoard(imageGraphics);
            paintBorder(imageGraphics);
            paintCloseIcon(imageGraphics);
            //
            if (boardModel.isBeforeFirstMove() && gameModel.playsHumanWhite() && !gameModel.hasPiece()) paintChangeColorIcon(imageGraphics);
            //
            imageGraphics.dispose();
            this.contentImage = contentImage;
        }
        //
        var bounds = graphics.getClipBounds();
        graphics.drawImage(contentImage, bounds.x, bounds.x, bounds.width, bounds.height, bounds.x, bounds.x, bounds.width, bounds.height, this);
    }
    //
    private void paintBoard(Graphics graphics)
    {
        Function<Integer, Integer> columnTransform, rowTransform;
        //
        if (gameModel.playsHumanWhite())
        {
            columnTransform = c -> BORDER + c * SQUARE_SIDE;
            rowTransform = r -> BORDER + (7 - r) * SQUARE_SIDE;
        }
        else
        {
            columnTransform = c -> BORDER + (7 - c) * SQUARE_SIDE;
            rowTransform = r -> BORDER + r * SQUARE_SIDE;
        }
        //
        for (int c = 0; c < 8; c++)
        {
            for (int r = 0; r < 8; r++)
            {
                var piece = boardModel.getPieceAt(c, r);
                var color = Position.squareColor(c, r);
                int x = columnTransform.apply(c);
                int y = rowTransform.apply(r);
                //
                if (isMarked(c, r, piece))
                {
                    setMarkColor(graphics, color);
                }
                else if (isAttackedNearKing(c, r))
                {
                    setAttackedColor(graphics, color);
                }
                else
                {
                    setSquareColor(graphics, color);
                }
                //
                paintSquare(graphics, x, y);
                //
                if (piece != null)
                {
                    var image = getImage(piece);
                    paintImage(graphics, x, y, image);
                }
            }
        }
    }
    //
    private static void paintBorder(Graphics graphics)
    {
        graphics.setColor(FRAME_COLOR);
        graphics.fillRect(0, 0, BOARD_SIDE + SQUARE_SIDE, BORDER - 2);
        graphics.fillRect(0, BORDER - 2, BORDER - 2, BOARD_SIDE + 4);
        graphics.fillRect(0, BOARD_SIDE + BORDER + 2, BOARD_SIDE + SQUARE_SIDE, BORDER - 2);
        graphics.fillRect(BOARD_SIDE + BORDER + 2, BORDER - 2, BORDER - 2, BOARD_SIDE + 4);
    }
    //
    private static void paintCloseIcon(Graphics graphics)
    {
        graphics.setColor(java.awt.Color.BLACK);
        graphics.fillPolygon(CROSS);
    }
    //
    private static void paintChangeColorIcon(Graphics graphics)
    {
        graphics.setColor(BLACK_SQUARE_COLOR);
        graphics.fillPolygon(ARROW);
    }
    //
    private boolean isMarked(int c, int r, Piece piece)
    {
        var takenPiece = gameModel.getPiece();
        var movedPiece = boardModel.getMovedPiece();
        //
        if (piece == null)
        {
            if (movedPiece == null) return false;
            //
            piece = movedPiece.getBefore();
            //
            if (piece == null) return false;
            //
            var beforePosition = piece.getPosition();
            //
            if (beforePosition == null) return false;
            //
            return beforePosition.is(c, r);
        }
        //
        return piece == movedPiece || piece == takenPiece;
    }
    //
    private boolean isAttackedNearKing(int c, int r)
    {
        var board = boardModel.getBoard();
        var king = board.getKing();
        var kingPosition = king.getPosition();
        //
        return isNear(c, r, kingPosition) && board.isAttacked(c, r);
    }
    //
    private static boolean isNear(int c, int r, Position position)
    {
        int dx = Math.abs(position.getX() - c);
        int dy = Math.abs(position.getY() - r);
        //
        return dx <= 1 && dy <= 1;
    }
    //
    private static void setSquareColor(Graphics graphics, Color color)
    {
        if (color == Color.WHITE)
        {
            graphics.setColor(WHITE_SQUARE_COLOR);
            //
            return;
        }
        //
        graphics.setColor(BLACK_SQUARE_COLOR);
    }
    //
    private static void setMarkColor(Graphics graphics, Color color)
    {
        if (color == Color.WHITE)
        {
            graphics.setColor(LIGHT_MARK_COLOR);
            //
            return;
        }
        //
        graphics.setColor(DARK_MARK_COLOR);
    }
    //
    private static void setAttackedColor(Graphics graphics, Color color)
    {
        if (color == Color.WHITE)
        {
            graphics.setColor(LIGHT_ATTACKED_COLOR);
            //
            return;
        }
        //
        graphics.setColor(DARK_ATTACKED_COLOR);
    }
    //
    private static void paintSquare(Graphics graphics, int x, int y)
    {
        graphics.fillRect(x, y, SQUARE_SIDE, SQUARE_SIDE);
    }
    //
    private Image getImage(Piece piece)
    {
        var name = getResourceName(piece);
        var image = imageByName.get(name);
        //
        if (image == null)
        {
            try
            {
                image = createImage(name);
                imageByName.put(name, image);
                var tracker = new MediaTracker(this);
                tracker.addImage(image, 0);
                tracker.waitForAll();
                //
                return image;
            }
            catch (InterruptedException e)
            {
                // do nothing
            }
        }
        //
        return image;
    }
    //
    private Image createImage(String name)
    {
        var clazz = getClass();
        var url = clazz.getResource(name);
        //
        try
        {
            var producer = (ImageProducer) url.getContent();
            //
            return createImage(producer);
        }
        catch (IOException e)
        {
            return null;
        }
    }
    //
    private static String getResourceName(Piece piece)
    {
        return "ICON_" + piece.getAbbreviatedName() + piece.getColor() + ".png";
    }
    //
    private void paintImage(Graphics graphics, int x, int y, Image image)
    {
        int w = image.getWidth(this);
        int h = image.getHeight(this);
        graphics.drawImage(image, x + (SQUARE_SIDE - w) / 2, y + (SQUARE_SIDE - h) / 2, this);
    }
    //
    @Override
    public Dimension getMinimumSize()
    {
        return new Dimension(CANVAS_SIDE, CANVAS_SIDE);
    }
    //
    @Override
    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }
    //
    private static Polygon createCross(int x, int y)
    {
        var polygon = new Polygon();
        polygon.addPoint(x + 2, y + 6);
        polygon.addPoint(x + 6, y + 2);
        polygon.addPoint(x + 10, y + 6);
        polygon.addPoint(x + 14, y + 2);
        polygon.addPoint(x + 18, y + 6);
        polygon.addPoint(x + 14, y + 10);
        polygon.addPoint(x + 18, y + 14);
        polygon.addPoint(x + 14, y + 18);
        polygon.addPoint(x + 10, y + 14);
        polygon.addPoint(x + 6, y + 18);
        polygon.addPoint(x + 2, y + 14);
        polygon.addPoint(x + 6, y + 10);
        //
        return polygon;
    }
    //
    private static Polygon createArrow(int x, int y)
    {
        var polygon = new Polygon();
        polygon.addPoint(x + 6, y);
        polygon.addPoint(x + 13, y);
        polygon.addPoint(x + 13, y + 9);
        polygon.addPoint(x + 20, y + 9);
        polygon.addPoint(x + 20, y + 5);
        polygon.addPoint(x + 29, y + 12);
        polygon.addPoint(x + 20, y + 18);
        polygon.addPoint(x + 20, y + 14);
        polygon.addPoint(x + 6, y + 14);
        //
        return polygon;
    }
}