package chess.search.opening;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import chess.move.Move;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
//
public final class OpeningService
{
    private Random random;
    //
    public OpeningService()
    {
        try
        {
            random = SecureRandom.getInstanceStrong();
        }
        catch (NoSuchAlgorithmException e)
        {
            random = new Random();
        }
    }
    //
    public Board lookup(Board board)
    {
        try
        {
            var qualifiedMoves = queryQualifiedMoves(board);
            //
            return applySelectedMove(board, qualifiedMoves);
        }
        catch (IOException e)
        {
            // ignore
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        //
        return null;
    }
    //
    private Board applySelectedMove(Board board, List<JSONObject> qualifiedMoves)
    {
        var bookMoves = new LinkedList<JSONObject>();
        JSONObject deepestMove = null;
        int maxAnalysisDepth = 0;
        //
        for (var qualifiedMove : qualifiedMoves)
        {
            int analysisDepth = qualifiedMove.optInt("analysisDepth");
            var openingLabels = qualifiedMove.optJSONArray("openingLabels");
            //
            if (analysisDepth > maxAnalysisDepth)
            {
                deepestMove = qualifiedMove;
                maxAnalysisDepth = analysisDepth;
            }
            //
            if (openingLabels != null && openingLabels.length() > 0)
            {
                bookMoves.addLast(qualifiedMove);
            }
        }
        //
        JSONObject selectedMove;
        //
        if (bookMoves.isEmpty())
        {
            if (deepestMove == null) return null;
            //
            selectedMove = deepestMove;
        }
        else
        {
            if (board.getBeforeBoard() == null) bookMoves.removeIf(OpeningService::isBadFirstMove);
            //
            Collections.shuffle(bookMoves, random);
            selectedMove = bookMoves.getFirst();
        }
        //
        var moveLabel = selectedMove.optString("moveLabel");
        var openingLabels = selectedMove.optJSONArray("openingLabels");
        var analysisDepth = selectedMove.optInt("analysisDepth");
        //
        if (moveLabel == null) return null;
        //
        var moves = board.generateMoves();
        //
        while (!moves.isEmpty())
        {
            var move = moves.poll();
            //
            if (moveLabel.equals(move.toString()))
            {
                board = board.applyMove(move);
                board.setComment(createComment(openingLabels, analysisDepth));
                //
                return board;
            }
        }
        //
        return null;
    }
    //
    private static boolean isBadFirstMove(JSONObject move)
    {
        var moveLabel = move.optString("moveLabel");
        // Königsfianchetto
        // Grob-Eröffnung
        return "g2-g3".equals(moveLabel) || "g2-g4".equals(moveLabel);
    }
    //
    private static String createComment(JSONArray openingLabels, int analysisDepth)
    {
        try
        {
            int openingLabelCount = openingLabels.length();
            //
            if (openingLabelCount > 0)
            {
                var firstLabel = openingLabels.getString(0);
                var comment = new StringBuilder();
                comment.append(firstLabel);
                //
                if (openingLabelCount >= 2) comment.append(String.format(" und %s weitere", openingLabelCount - 1));
                //
                return comment.toString();
            }
            else if (analysisDepth > 0)
            {
                return String.format("Analysetiefe %s", analysisDepth);
            }
        }
        catch (JSONException e)
        {
            // pass
        }
        //
        return null;
    }
    //
    private static List<JSONObject> queryQualifiedMoves(Board board) throws IOException
    {
        var url = getNextMovesURL(board);
        var connection = open(url);
        var content = read(connection);
        //
        try
        {
            var array = new JSONArray(content);
            var list = new LinkedList<JSONObject>();
            //
            for (int i = 0; i < array.length(); i++)
            {
                list.addLast(array.getJSONObject(i));
            }
            //
            return list;
        }
        catch (JSONException e)
        {
            throw new IOException(e);
        }
    }
    //
    private static URL getNextMovesURL(Board board) throws IOException
    {
        var moves = new LinkedList<Move>();
        //
        while (board != null)
        {
            var beforeBoard = board.getBeforeBoard();
            var beforeMove = board.getBeforeMove();
            //
            if (beforeMove != null) moves.addFirst(beforeMove);
            //
            board = beforeBoard;
        }
        //
        var resource = new StringBuilder();
        resource.append("https://chess-database-service-3hbx73e5ra-ey.a.run.app/nextMoves?line=");
        //
        for (var move : moves)
        {
            resource.append(URLEncoder.encode(move.toString(), StandardCharsets.UTF_8));
            resource.append('+');
        }
        //
        if (!moves.isEmpty()) resource.setLength(resource.length() - 1);
        //
        return new URL(resource.toString());
    }
    //
    private static HttpURLConnection open(URL url) throws IOException
    {
        var connection = url.openConnection();
        connection.setDoInput(true);
        //
        return (HttpURLConnection) connection;
    }
    //
    private static String read(HttpURLConnection connection) throws IOException
    {
        int code = connection.getResponseCode();
        //
        if (code > 299) throw new IOException("code " + code);
        //
        try (InputStream stream = connection.getInputStream())
        {
            byte[] bytes = stream.readAllBytes();
            //
            return new String(bytes, StandardCharsets.UTF_8);
        }
    }
}
