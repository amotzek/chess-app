package chess.search.opening;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import java.util.HashMap;
import java.util.HashSet;
//
final class MoveTree
{
    private final Board board;
    private final HashMap<String, MoveTree> childByMoveName;
    private final HashSet<String> openingNames;
    //
    public MoveTree(Board board)
    {
        super();
        //
        this.board = board;
        //
        childByMoveName = new HashMap<>();
        openingNames = new HashSet<>();
    }
    //
    public Board getBoard()
    {
        return board;
    }
    //
    public void addChild(String moveName, MoveTree child, String openingName)
    {
        childByMoveName.put(moveName, child);
        openingNames.add(openingName);
    }
    //
    public int size()
    {
        return childByMoveName.size();
    }
    //
    public String getMoveName(int index)
    {
        var moveNames = childByMoveName.keySet().iterator();
        //
        while (index > 0)
        {
            moveNames.next();
            index--;
        }
        //
        return moveNames.next();
    }
    //
    public String getOpeningName()
    {
        if (openingNames.size() == 1) return openingNames.iterator().next();
        //
        return null;
    }
 }