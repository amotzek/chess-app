package chess.search.opening;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Logger;
import chess.board.Board;
/*
 * Created by andreasm on 28.05.2014.
 */
public final class OpeningBook
{
    private static final Logger logger = Logger.getLogger("OpeningBook");
    private static final Random random = new Random();
    //
    private final MoveTree root;
    private final HashMap<Board, MoveTree> treeByBoard;
    //
    public OpeningBook()
    {
        super();
        //
        var board = new Board();
        root = new MoveTree(board);
        treeByBoard = new HashMap<>();
        treeByBoard.put(board, root);
        //
        try
        {
            var url = getClass().getResource("opening-book.txt");
            var input = new InputStreamReader(url.openStream(), StandardCharsets.UTF_8);
            var reader = new BufferedReader(input);
            loadFrom(reader);
        }
        catch (IOException e)
        {
            // do nothing
        }
    }
    //
    private void loadFrom(BufferedReader reader)
    {
        var opening = new LinkedList<String>();
        //
        try
        {
            while (true)
            {
                var line = reader.readLine();
                //
                if (line == null) break;
                //
                if (line.charAt(0) != '#')
                {
                    opening.addLast(line);
                }
                else
                {
                    var openingName = line.substring(1);
                    addMoves(opening, openingName);
                    opening.clear();
                }
            }
            //
            int count = 0;
            //
            for (var tree : treeByBoard.values())
            {
                if (tree.size() > 0) count++;
            }
            //
            logger.info("opening book has moves for " + count +  " boards");
        }
        catch (IOException e)
        {
            // do nothing
        }
    }
    //
    private void addMoves(LinkedList<String> moveNames, String openingName)
    {
        var tree = root;
        //
        while (!moveNames.isEmpty())
        {
            var board = tree.getBoard();
            var moveName = moveNames.removeFirst();
            var nextBoard = applyMove(board, moveName);
            //
            if (nextBoard == null)
            {
                var builder = new StringBuilder();
                builder.append("move ");
                builder.append(moveName);
                builder.append(" not possible in ");
                builder.append(openingName);
                builder.append(" for board ");
                builder.append(board);
                builder.append(" with moves");
                var moves = board.generateMoves();
                //
                while (!moves.isEmpty())
                {
                    builder.append(" ");
                    builder.append(moves.poll());
                }
                //
                logger.warning(builder.toString());
                //
                return;
            }
            //
            var nextTree = treeByBoard.get(nextBoard);
            //
            if (nextTree == null)
            {
                nextTree = new MoveTree(nextBoard);
                treeByBoard.put(nextBoard, nextTree);
            }
            //
            tree.addChild(moveName, nextTree, openingName);
            tree = nextTree;
        }
    }
    //
    public Board lookup(Board board)
    {
        var tree = treeByBoard.get(board);
        //
        if (tree == null) return null;
        //
        int size = tree.size();
        //
        if (size == 0) return null;
        //
        int choice = (size > 1) ? random.nextInt(size) : 0;
        var moveName = tree.getMoveName(choice);
        var openingName = tree.getOpeningName();
        var nextBoard = applyMove(board, moveName);
        nextBoard.setComment(openingName);
        //
        return nextBoard;
    }
    //
    private static Board applyMove(Board board, String moveName1)
    {
        var moves = board.generateMoves();
        //
        while (!moves.isEmpty())
        {
            var move = moves.poll();
            var moveName2 = move.toString();
            //
            if (moveName1.equals(moveName2)) return board.applyMove(move);
        }
        //
        return null;
    }
}