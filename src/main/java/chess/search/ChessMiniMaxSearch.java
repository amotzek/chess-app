package chess.search;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import java.util.List;
import chess.board.Board;
import ai.search.tree.minimax.MiniMaxSearch;
import chess.move.Move;
/*
 * Created by andreasm 27.05.14 20:33
 */
public final class ChessMiniMaxSearch extends MiniMaxSearch<Board>
{
    private static final double[] aspirationFactors = { 1d, 8d, 15d, 29d }; // Halber Bauer, Springer, Turm, Dame
    //
    private final Board startBoard;
    private final double[] weights;
    private final int startDepth;
    private final int endDepth;
    //
    public ChessMiniMaxSearch(Board startBoard, double[] weights)
    {
        super();
        //
        this.startBoard = startBoard;
        this.weights = weights;
        //
        int count = 0;
        var board = startBoard;
        //
        do
        {
            board = board.getNextBoard();
            count++;
        }
        while (board != null);
        //
        startDepth = 2;
        endDepth = Math.max(count + 5, 32);
    }
    //
    protected Board getStart()
    {
        return startBoard;
    }
    //
    protected int getStartDepth()
    {
        return startDepth;
    }
    //
    protected int getEndDepth()
    {
        return endDepth;
    }
    //
    public Board[] findSuccessors(Board board)
    {
        var moves = board.generateMoves();
        int size = moves.size();
        var boards = new Board[size];
        int index = 0;
        //
        while (!moves.isEmpty())
        {
            var move = moves.poll();
            boards[index++] = board.applyMove(move);
        }
        //
        return boards;
    }
    //
    public double getValue(int depth, Board board)
    {
        return board.getStaticValue(depth, weights);
    }
    //
    protected boolean isQuiesce(int depth, Board board)
    {
        var beforeBoard = board.getBeforeBoard();
        var beforeMove = board.getBeforeMove();
        //
        if (beforeBoard == null || beforeMove == null) return true;
        //
        if (beforeMove.isNull()) return false;
        //
        if (beforeMove.isCapture() || beforeMove.isPromotion()) return depth < -6;
        //
        if (beforeBoard.isKingUnderAttack()) return depth < -10;
        //
        return true;
    }
    //
    protected double getAspirationWindowSize(int fails)
    {
        if (fails >= 4) return Double.MAX_VALUE;
        //
        return weights[5] * aspirationFactors[fails];
    }
    //
    protected void onIntermediatePath(List<Board> path)
    {
        var beforeBoard = path.get(0).getBeforeBoard();
        //
        for (var board : path)
        {
            var nextBoard = transposeBoard(beforeBoard, board);
            //
            if (nextBoard == null) break;
            //
            beforeBoard.setNextBoard(nextBoard);
            beforeBoard = nextBoard;
        }
    }
    //
    private static Board transposeBoard(Board beforeBoard, Board board)
    {
        if (board.getBeforeBoard() == beforeBoard) return board;
        //
        var moves = beforeBoard.generateMoves();
        //
        if (moves.isEmpty()) return null; // Endstellung
        //
        while (!moves.isEmpty())
        {
            var move = moves.poll();
            var nextBoard = beforeBoard.applyMove(move);
            //
            if (board.equalPosition(nextBoard)) return nextBoard; // nextBoard.hasEnded() kann true sein
        }
        //
        throw new IllegalArgumentException(String.format("cannot transpose board to line %s", toLine(beforeBoard)));
    }
    //
    private static LinkedList<Move> toLine(Board board)
    {
        var line = new LinkedList<Move>();
        //
        while (board != null)
        {
            line.addFirst(board.getBeforeMove());
            board = board.getBeforeBoard();
        }
        //
        line.removeFirst();
        //
        return line;
    }
    //
    public void close()
    {
    }
}