package chess.model;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import chess.board.Board;
import chess.board.Color;
import chess.move.Move;
import chess.piece.Piece;
/*
 * Created by andreasm 27.05.14 19:16
 */
public final class GameModel
{
    private final LinkedList<GameModelListener> listeners;
    private Color humanColor;
    private Color playerColor;
    private Piece piece;
    //
    public GameModel()
    {
        listeners = new LinkedList<>();
        humanColor = Color.WHITE;
        playerColor = Color.WHITE;
    }
    //
    public void addListener(GameModelListener listener)
    {
        listeners.addLast(listener);
    }
    //
    public Color getHumanColor()
    {
        return humanColor;
    }
    //
    public void setHumanColorBlack()
    {
        humanColor = Color.BLACK;
        //
        for (var listener : listeners)
        {
            listener.playerChanged(playerColor, humanColor);
        }
    }
    //
    public boolean playsHumanWhite()
    {
        return humanColor == Color.WHITE;
    }
    //
    public Color getPlayerColor()
    {
        return playerColor;
    }
    //
    public boolean hasPiece()
    {
        return piece != null;
    }
    //
    public Piece getPiece()
    {
        return piece;
    }
    //
    public void takePiece(Piece piece)
    {
        if (playerColor != humanColor) throw new IllegalStateException();
        //
        this.piece = piece;
        //
        for (var listener : listeners)
        {
            listener.pieceTaken(piece);
        }
    }
    //
    public void putPiece(Move move)
    {
        if (playerColor != humanColor) throw new IllegalStateException();
        //
        piece = null;
        //
        for (var listener : listeners)
        {
            listener.piecePut(move);
        }
        //
        playerColor = Color.getOpposite(playerColor);
        //
        for (var listener : listeners)
        {
            listener.playerChanged(playerColor, humanColor);
        }
    }
    //
    public void clearPiece()
    {
        if (playerColor != humanColor) throw new IllegalStateException();
        //
        piece = null;
        //
        for (var listener : listeners)
        {
            listener.pieceCleared();
        }
    }
    //
    public void setBoard(Board board)
    {
        var boardColor = board.getColor();
        //
        if (playerColor == boardColor) throw new IllegalStateException();
        //
        var comment = board.getComment();
        var move = board.getBeforeMove();
        var piece = move.getFirstPiece().getBefore();
        //
        for (var listener : listeners)
        {
            listener.pieceTaken(piece);
        }
        //
        for (var listener : listeners)
        {
            listener.piecePut(move);
        }
        //
        playerColor = boardColor;
        //
        for (var listener : listeners)
        {
            listener.playerChanged(playerColor, humanColor);
        }
        //
        if (comment == null) return;
        //
        for (var listener : listeners)
        {
            listener.info(comment);
        }
    }
}