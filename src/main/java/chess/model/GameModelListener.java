package chess.model;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Color;
import chess.move.Move;
import chess.piece.Piece;
/*
 * Created by andreasm 27.05.14 20:01
 */
public interface GameModelListener
{
    void playerChanged(Color playerColor, Color humanColor);
    //
    void pieceCleared();
    //
    void pieceTaken(Piece piece);
    //
    void piecePut(Move move);
    //
    void info(String message);
}