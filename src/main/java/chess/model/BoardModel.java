package chess.model;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import chess.board.Board;
import chess.move.Move;
import chess.move.MoveQueue;
import chess.piece.Piece;
/*
 * Created by andreasm on 27.05.2014.
 */
public final class BoardModel
{
    private final LinkedList<BoardModelListener> listeners;
    private Board board;
    //
    public BoardModel(Board board)
    {
        this.board = board;
        //
        listeners = new LinkedList<>();
        board.generateMoves();
    }
    //
    public void addListener(BoardModelListener listener)
    {
        listeners.addLast(listener);
    }
    //
    public Board getBoard()
    {
        return board;
    }
    //
    public void setBoard(Board board)
    {
        if (board == null) throw new IllegalArgumentException();
        //
        this.board = board;
        //
        board.generateMoves();
        //
        for (var listener : listeners)
        {
            listener.boardModelChanged();
        }
    }
    //
    public Piece getPieceAt(int x, int y)
    {
        return board.getPieceAt(x, y);
    }
    //
    public MoveQueue getMoves()
    {
        return board.generateMoves();
    }
    //
    public void doMove(Move move)
    {
        setBoard(board.applyMove(move));
    }
    //
    public Piece getMovedPiece()
    {
        var move = board.getBeforeMove();
        //
        if (move == null) return null;
        //
        return move.getFirstPiece();
    }
    //
    public boolean isBeforeFirstMove()
    {
        return board.getBeforeMove() == null;
    }
}