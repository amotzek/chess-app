package chess.move;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.piece.King;
import chess.piece.Pawn;
import chess.piece.Piece;
import chess.piece.Rook;
/*
 * Created by andreasm on 26.05.2014.
 */
public abstract class MoveQueue
{
    private static final Null NULL = new Null();
    //
    public abstract boolean isEmpty();
    //
    public abstract int size();
    //
    public abstract Move poll();
    //
    public final void offerMove(Piece piece)
    {
        offer(new Move(piece));
    }
    //
    public final void offerCapture(Piece attacker, Piece attacked)
    {
        offer(new Capture(attacker, attacked));
    }
    //
    public final void offerCastling(King king, Rook rook)
    {
        offer(new Castling(king, rook));
    }
    //
    public final void offerEnPassant(Pawn attacker, Pawn attacked)
    {
        offer(new EnPassant(attacker, attacked));
    }
    //
    public final void offerPromotion(Piece piece)
    {
        offer(new Promotion(piece));
    }
    //
    public final void offerPromotionAndCapture(Piece attacker, Piece attacked)
    {
        offer(new PromotionAndCapture(attacker, attacked));
    }
    //
    public final void offerNull()
    {
        offer(NULL);
    }
    //
    protected abstract void offer(Move move);
}
