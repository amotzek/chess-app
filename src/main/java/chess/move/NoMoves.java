/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
package chess.move;
/*
 * Created by andreasm on 28.05.2014.
 */
public final class NoMoves extends MoveQueue
{
    public boolean isEmpty()
    {
        return true;
    }
    //
    public int size()
    {
        return 0;
    }
    //
    public Move poll()
    {
        throw new UnsupportedOperationException();
    }
    //
    protected void offer(Move move)
    {
        throw new UnsupportedOperationException();
    }
}