package chess.move;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.Comparator;
import java.util.PriorityQueue;
//
public final class OrderedMoves extends MoveQueue
{
    private final PriorityQueue<Move> queue;
    //
    public OrderedMoves(Comparator<Move> comparator)
    {
        super();
        //
        queue = new PriorityQueue<>(40, comparator);
    }
    //
    public boolean isEmpty()
    {
        return queue.isEmpty();
    }
    //
    public int size()
    {
        return queue.size();
    }
    //
    protected void offer(Move move)
    {
        queue.offer(move);
    }
    //
    public Move poll()
    {
        return queue.poll();
    }
}