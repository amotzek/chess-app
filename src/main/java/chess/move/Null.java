package chess.move;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
public final class Null extends Move
{
    public Null()
    {
        super(null);
    }
    //
    @Override
    public boolean isNull()
    {
        return true;
    }
    //
    @Override
    public String toString()
    {
        return "-";
    }
    //
    @Override
    public int hashCode()
    {
        return 0;
    }
}
