package chess.move;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.piece.Piece;
/*
 * Created by andreasm on 26.05.2014.
 */
final class Castling extends Move
{
    public Castling(Piece first, Piece second)
    {
        super(first, second);
    }
    //
    public boolean isCastling()
    {
        return true;
    }
    //
    @Override
    public String toString()
    {
        if (second.getBefore().getPosition().getX() == 0) return "O-O-O";
        //
        return "O-O";
    }
}