package chess.move;
/*
 * Copyright (C) 1998, 2020, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.piece.Pawn;
import chess.piece.Piece;
/*
 * Created by andreasm on 26.05.2014.
 */
public class Move
{
    protected final Piece first;
    protected final Piece second;
    //
    Move(Piece first)
    {
        this(first, null);
    }
    //
    protected Move(Piece first, Piece second)
    {
        this.first = first;
        this.second = second;
    }
    //
    public final Piece getFirstPiece()
    {
        return first;
    }
    //
    public final Piece getSecondPiece()
    {
        return second;
    }
    //
    public boolean isCapture()
    {
        return false;
    }
    //
    public boolean isPromotion()
    {
        return false;
    }
    //
    public boolean involvesPawnOrIsCapture()
    {
        return first instanceof Pawn;
    }
    //
    public boolean isCastling()
    {
        return false;
    }
    //
    public boolean isNull()
    {
        return false;
    }
    //
    @Override
    public String toString()
    {
        return first.getBefore().toString() + "-" + first.getPosition().toString();
    }
    //
    @Override
    public final boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (o == null || getClass() != o.getClass()) return false;
        //
        if (o instanceof Null) return false;
        //
        var move = (Move) o;
        //
        if (!first.equals(move.first)) return false;
        //
        if (!first.getBefore().equals(move.first.getBefore())) return false;
        //
        if (second == null) return move.second == null;
        //
        return second.equals(move.second);
    }
    //
    @Override
    public int hashCode()
    {
        int result = first.hashCode();
        result = 31 * result + (second != null ? second.hashCode() : 0);
        //
        return result;
    }
}
