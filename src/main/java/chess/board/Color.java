/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
package chess.board;
/*
 * Created by andreasm on 26.05.2014.
 */
public enum Color
{
    BLACK,
    WHITE;
    //
    public static Color getOpposite(Color color)
    {
        if (color == BLACK) return WHITE;
        //
        return BLACK;
    }
}