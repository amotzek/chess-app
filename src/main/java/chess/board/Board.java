package chess.board;
/*
 * Copyright (C) 1998, 2020, 2021, 2022 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import ai.search.tree.minimax.MiniMaxSearch;
import chess.move.Move;
import chess.move.MoveQueue;
import chess.move.NoMoves;
import chess.move.OrderedMoves;
import chess.piece.Bishop;
import chess.piece.King;
import chess.piece.Knight;
import chess.piece.Pawn;
import chess.piece.Piece;
import chess.piece.Queen;
import chess.piece.Rook;
// import jdk.incubator.vector.DoubleVector;
// import jdk.incubator.vector.VectorOperators;
//
public final class Board
{
    public static final double[] STANDARD_PARAMETERS = new double[] {
            8d, // Doppelbauern
            2d, // Promotion der Bauern (Endspiel)
            1d, // Zentralität der Bauern (Eröffnung)
            1d, // Zentralität der Damen und Könige (Endspiel)
            17d, // Zentralität der Springer, Läufer und Türme
            9.06666d, // Königsangriffe
            9.6d, // Königsverteidigung
            40d, // Rochaden (Eröffnung)
            5000d }; // Figurenwerte
    //
    private static final NoMoves NO_MOVES = new NoMoves();
    //
    private final Board beforeBoard;
    private final Move beforeMove;
    private final int movesWithoutPawnAndCapture;
    private final int castlings;
    private final Color color;
    private final Piece[] pieces;
    private King whiteKing;
    private King blackKing;
    private Board nextBoard;
    private Move nextMove;
    private ArrayList<Position>[] attacksFrom;
    private boolean[] attacksTo;
    private int[] defence;
    private String comment;
    private int hash;
    //
    public Board()
    {
        beforeBoard = null;
        beforeMove = null;
        movesWithoutPawnAndCapture = 0;
        castlings = 0;
        color = Color.WHITE;
        pieces = new Piece[64];
        place(new Rook('a', 1, Color.WHITE, 0));
        place(new Knight('b', 1, Color.WHITE, 1));
        place(new Bishop('c', 1, Color.WHITE, 2));
        place(new Queen('d', 1, Color.WHITE, 3));
        place(new King('e', 1, Color.WHITE, 4));
        place(new Bishop('f', 1, Color.WHITE, 5));
        place(new Knight('g', 1, Color.WHITE, 6));
        place(new Rook('h', 1, Color.WHITE, 7));
        place(new Pawn('a', 2, Color.WHITE, 8));
        place(new Pawn('b', 2, Color.WHITE, 9));
        place(new Pawn('c', 2, Color.WHITE, 10));
        place(new Pawn('d', 2, Color.WHITE, 11));
        place(new Pawn('e', 2, Color.WHITE, 12));
        place(new Pawn('f', 2, Color.WHITE, 13));
        place(new Pawn('g', 2, Color.WHITE, 14));
        place(new Pawn('h', 2, Color.WHITE, 15));
        place(new Rook('a', 8, Color.BLACK, 16));
        place(new Knight('b', 8, Color.BLACK, 17));
        place(new Bishop('c', 8, Color.BLACK, 18));
        place(new Queen('d', 8, Color.BLACK, 19));
        place(new King('e', 8, Color.BLACK, 20));
        place(new Bishop('f', 8, Color.BLACK, 21));
        place(new Knight('g', 8, Color.BLACK, 22));
        place(new Rook('h', 8, Color.BLACK, 23));
        place(new Pawn('a', 7, Color.BLACK, 24));
        place(new Pawn('b', 7, Color.BLACK, 25));
        place(new Pawn('c', 7, Color.BLACK, 26));
        place(new Pawn('d', 7, Color.BLACK, 27));
        place(new Pawn('e', 7, Color.BLACK, 28));
        place(new Pawn('f', 7, Color.BLACK, 29));
        place(new Pawn('g', 7, Color.BLACK, 30));
        place(new Pawn('h', 7, Color.BLACK, 31));
    }
    //
    private void place(Piece piece)
    {
        if (piece == null) return;
        //
        var position = piece.getPosition();
        //
        if (position == null) return;
        //
        int x = position.getX();
        int y = position.getY();
        int fieldIndex = XYtoFieldIndex(x, y);
        //
        assert pieces[fieldIndex] == null;
        //
        pieces[fieldIndex] = piece;
        //
        if (piece instanceof King)
        {
            if (piece.getColor() == Color.WHITE)
            {
                whiteKing = (King) piece;
            }
            else
            {
                blackKing = (King) piece;
            }
        }
    }
    //
    public Board applyMove(Move move)
    {
        if (move.equals(nextMove)) return nextBoard;
        //
        return new Board(this, move);
    }
    //
    private Board(Board beforeBoard, Move beforeMove)
    {
        this.beforeBoard = beforeBoard;
        this.beforeMove = beforeMove;
        //
        color = Color.getOpposite(beforeBoard.color);
        //
        if (beforeMove.involvesPawnOrIsCapture())
        {
            movesWithoutPawnAndCapture = 0;
            castlings = beforeBoard.castlings;
        }
        else if (beforeMove.isCastling())
        {
            movesWithoutPawnAndCapture = beforeBoard.movesWithoutPawnAndCapture + 1;
            //
            if (color == Color.WHITE)
            {
                castlings = beforeBoard.castlings - 1; // Schwarz hat rochiert
            }
            else
            {
                castlings = beforeBoard.castlings + 1; // Weiss hat rochiert
            }
        }
        else
        {
            movesWithoutPawnAndCapture = beforeBoard.movesWithoutPawnAndCapture + 1;
            castlings = beforeBoard.castlings;
        }
        //
        pieces = new Piece[64];
        whiteKing = beforeBoard.whiteKing;
        blackKing = beforeBoard.blackKing;
        System.arraycopy(beforeBoard.pieces, 0, pieces, 0, 64);
        //
        unplace(beforeMove.getFirstPiece());
        unplace(beforeMove.getSecondPiece());
        place(beforeMove.getFirstPiece());
        place(beforeMove.getSecondPiece());
    }
    //
    private void unplace(Piece piece)
    {
        if (piece == null) return;
        //
        var beforePiece = piece.getBefore();
        var beforePosition = beforePiece.getPosition();
        int x = beforePosition.getX();
        int y = beforePosition.getY();
        int fieldIndex = XYtoFieldIndex(x, y);
        //
        assert pieces[fieldIndex] == beforePiece;
        //
        pieces[fieldIndex] = null;
        //
        if (piece instanceof King)
        {
            if (piece.getColor() == Color.WHITE)
            {
                whiteKing = null;
            }
            else
            {
                blackKing = null;
            }
        }
    }
    //
    public Color getColor()
    {
        return color;
    }
    //
    public Board getBeforeBoard()
    {
        return beforeBoard;
    }
    //
    public Move getBeforeMove()
    {
        return beforeMove;
    }
    //
    public void setNextBoard(Board board)
    {
        nextMove = board.beforeMove;
        nextBoard = board;
    }
    //
    public Board getNextBoard()
    {
        return nextBoard;
    }
    //
    public String getComment()
    {
        return comment;
    }
    //
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    //
    public MoveQueue generateMoves()
    {
        if (hasEnded()) return NO_MOVES;
        //
        addAttacks();
        var comparator = new MoveComparator(beforeMove, nextMove, defence, attacksTo);
        var moves = new OrderedMoves(comparator);
        //
        for (var piece : pieces)
        {
            if (piece == null) continue;
            //
            piece.generateMoves(this, moves);
        }
        //
        if (moves.isEmpty()) moves.offerNull();
        //
        return moves;
    }
    //
    private boolean enoughMovesWithoutPawnAndCapture()
    {
        return movesWithoutPawnAndCapture >= 100;
    }
    //
    private boolean threefoldRepetition()
    {
        if (movesWithoutPawnAndCapture < 6) return false;
        //
        int count = 1;  // this
        var board = this;
        //
        for (int i = movesWithoutPawnAndCapture; i >= 2; i -= 2)
        {
            board = board.getBeforeBoard();
            board = board.getBeforeBoard();
            //
            if (equalPosition(board))
            {
                count++;
                //
                if (count >= 3) return true;
            }
        }
        //
        return false;
    }
    //
    private boolean followsNullMove()
    {
        var board = getBeforeBoard();
        //
        if (board == null) return false;
        //
        var move = board.getBeforeMove();
        //
        if (move == null) return false;
        //
        return move.isNull();
    }
    //
    private boolean anyKingWasCaptured()
    {
        return whiteKing == null || blackKing == null;
    }
    //
    public boolean hasEnded()
    {
        return enoughMovesWithoutPawnAndCapture()
            || threefoldRepetition()
            || followsNullMove()
            || anyKingWasCaptured();
    }
    //
    public Color getWinner()
    {
        if (whiteKing == null) return Color.BLACK;
        //
        if (blackKing == null) return Color.WHITE;
        //
        return null;
    }
    //
    @SuppressWarnings("unchecked")
    private synchronized void addAttacks() // muss synchronized sein, wenn eine Transposition Table benutzt wird, weil equals addAttacks aufruft
    {
        if (attacksFrom != null) return;
        //
        attacksFrom = new ArrayList[32];
        attacksTo = new boolean[64];
        defence = new int[64];
        //
        for (var piece : pieces)
        {
            if (piece == null) continue;
            //
            piece.addAttacks(this);
        }
    }
    //
    public King getKing()
    {
        return color == Color.WHITE ? whiteKing : blackKing;
    }
    //
    public boolean isKingUnderAttack()
    {
        var king = getKing();
        //
        if (king == null) return false;
        //
        addAttacks();
        //
        return isAttacked(king.getPosition());
    }
    //
    public boolean addAttack(Piece piece, int x, int y)
    {
        int fieldIndex = XYtoFieldIndex(x, y);
        //
        if (piece.getColor() == color)
        {
            int pieceIndex = piece.getIndex();
            var positions = attacksFrom[pieceIndex];
            //
            if (positions == null)
            {
                positions = new ArrayList<>(16);
                attacksFrom[pieceIndex] = positions;
            }
            //
            positions.add(Position.getInstance(x, y));
            //
            if (defence[fieldIndex] == 0 || defence[fieldIndex] > piece.getValue()) defence[fieldIndex] = piece.getValue();
        }
        else
        {
            attacksTo[fieldIndex] = true;
        }
        //
        return pieces[fieldIndex] == null;
    }
    //
    public List<Position> getAttacksFrom(Piece piece)
    {
        return attacksFrom[piece.getIndex()];
    }
    //
    public boolean isAttacked(int x, int y)
    {
        return attacksTo[XYtoFieldIndex(x, y)];
    }
    //
    private boolean isAttacked(Position position)
    {
        return isAttacked(position.getX(), position.getY());
    }
    //
    public Piece getPieceAt(int x, int y)
    {
        return pieces[XYtoFieldIndex(x, y)];
    }
    //
    public boolean isEmpty(int x, int y)
    {
        return getPieceAt(x, y) == null;
    }
    //
    public boolean canCapture(int x, int y)
    {
        var piece = getPieceAt(x, y);
        //
        if (piece == null) return false;
        //
        return piece.getColor() != color;
    }
    //
    static int XYtoFieldIndex(int x, int y)
    {
        return 8 * x + y;
    }
    //
    public double getStaticValue(int depth, double[] weights)
    {
        if (whiteKing == null) return color == Color.WHITE ? MiniMaxSearch.LOST - depth : MiniMaxSearch.WON + depth;
        //
        if (blackKing == null) return color == Color.BLACK ? MiniMaxSearch.LOST - depth : MiniMaxSearch.WON + depth;
        //
        if (enoughMovesWithoutPawnAndCapture() || threefoldRepetition() || followsNullMove()) return MiniMaxSearch.DRAW;
        //
        var whitePawnColumns = new BitSet();
        var blackPawnColumns = new BitSet();
        var whiteKingPosition = whiteKing.getPosition();
        var blackKingPosition = blackKing.getPosition();
        int whitePieceCount = 0;
        int blackPieceCount = 0;
        int pawnMultiplicityValue = 0;
        int pawnPromotionValue = 0;
        int kingSafetyValue = 0;
        int kingAttackValue = 0;
        int pawnCentralityValue = 0;
        int queenKingCentralityValue = 0;
        int knightBishopRookCentralityValue = 0;
        int pieceValue = 0;
        //
        for (var piece : pieces)
        {
            if (piece == null) continue;
            //
            var piecePosition = piece.getPosition();
            var pieceColor = piece.getColor();
            //
            if (pieceColor == Color.WHITE)
            {
                if (piece instanceof Pawn)
                {
                    pawnPromotionValue += eighthRankProximity(piecePosition); // besser in der Nähe der schwarzen Grundlinie
                    pawnCentralityValue += centrality(piecePosition); // besser in der Nähe des Brettzentrums
                    kingSafetyValue += ownKingProximity(piecePosition, whiteKingPosition); // besser in der Nähe des weißen Königs
                    var pawnColumn = piecePosition.getX();
                    //
                    if (whitePawnColumns.get(pawnColumn))
                    {
                        pawnMultiplicityValue--; // schlechter aus Sicht von Weiß
                    }
                    else
                    {
                        whitePawnColumns.set(pawnColumn);
                    }
                }
                else if (piece instanceof Rook)
                {
                    knightBishopRookCentralityValue += horizontalCentrality(piecePosition); // besser in der Nähe des Brettzentrums
                    kingSafetyValue += ownKingProximity(piecePosition, whiteKingPosition); // besser in der Nähe des weißen Königs
                    kingAttackValue += opponentRookKingProximity(piecePosition, blackKingPosition); // besser in der Nähe des schwarzen Königs
                }
                else if (piece instanceof Bishop)
                {
                    knightBishopRookCentralityValue += centrality(piecePosition); // besser in der Nähe des Brettzentrums
                    kingAttackValue += opponentBishopKingProximity(piecePosition, blackKingPosition); // besser in der Nähe des schwarzen Königs
                }
                else if (piece instanceof Knight)
                {
                    knightBishopRookCentralityValue += centrality(piecePosition); // besser in der Nähe des Brettzentrums
                    kingAttackValue += opponentKnightKingProximity(piecePosition, blackKingPosition); // besser in der Nähe des schwarzen Königs
                }
                else if (piece instanceof King)
                {
                    queenKingCentralityValue += centrality(piecePosition); // besser in der Nähe des Brettzentrums
                    kingAttackValue += opponentKingKingProximity(piecePosition, blackKingPosition); // besser in der Nähe des schwarzen Königs
                }
                else // piece instanceof Queen
                {
                    queenKingCentralityValue += centrality(piecePosition); // besser in der Nähe des Brettzentrums
                    kingAttackValue += opponentQueenKingProximity(piecePosition, blackKingPosition); // besser in der Nähe des schwarzen Königs
                }
                //
                pieceValue += piece.getValue(); // Figurenwert
                whitePieceCount++;
            }
            else
            {
                if (piece instanceof Pawn)
                {
                    pawnPromotionValue -= firstRankProximity(piecePosition); // schlechter in der Nähe der weißen Grundlinie
                    pawnCentralityValue -= centrality(piecePosition); // schlechter in der Nähe des Brettzentrums
                    kingSafetyValue -= ownKingProximity(piecePosition, blackKingPosition); // schlechter in der Nähe des schwarzen Königs
                    var pawnColumn = piecePosition.getX();
                    //
                    if (blackPawnColumns.get(pawnColumn))
                    {
                        pawnMultiplicityValue++; // besser aus Sicht von Weiß
                    }
                    else
                    {
                        blackPawnColumns.set(pawnColumn);
                    }
                }
                else if (piece instanceof Rook)
                {
                    knightBishopRookCentralityValue -= horizontalCentrality(piecePosition); // schlechter in der Nähe des Brettzentrums
                    kingSafetyValue -= ownKingProximity(piecePosition, blackKingPosition); // schlechter in der Nähe des schwarzen Königs
                    kingAttackValue -= opponentRookKingProximity(piecePosition, whiteKingPosition); // schlechter in der Nähe des weißen Königs
                }
                else if (piece instanceof Bishop)
                {
                    knightBishopRookCentralityValue -= centrality(piecePosition); // schlechter in der Nähe des Brettzentrums
                    kingAttackValue -= opponentBishopKingProximity(piecePosition, whiteKingPosition); // schlechter in der Nähe des weißen Königs
                }
                else if (piece instanceof Knight)
                {
                    knightBishopRookCentralityValue -= centrality(piecePosition); // schlechter in der Nähe des Brettzentrums
                    kingAttackValue -= opponentKnightKingProximity(piecePosition, whiteKingPosition); // schlechter in der Nähe des weißen Königs
                }
                else if (piece instanceof King)
                {
                    queenKingCentralityValue -= centrality(piecePosition); // schlechter in der Nähe des Brettzentrums
                    kingAttackValue -= opponentKingKingProximity(piecePosition, whiteKingPosition); // schlechter in der Nähe des weißen Königs
                }
                else // piece instanceof Queen
                {
                    queenKingCentralityValue -= centrality(piecePosition); // schlechter in der Nähe des Brettzentrums
                    kingAttackValue -= opponentQueenKingProximity(piecePosition, whiteKingPosition); // schlechter in der Nähe des weißen Königs
                }
                //
                pieceValue -= piece.getValue(); // Figurenwert
                blackPieceCount++;
            }
        }
        //
        int minPieceCount = Math.min(whitePieceCount, blackPieceCount);
        int maxPieceCount = Math.min(whitePieceCount, blackPieceCount);
        double pawnMultiplicityWeight = weights[0];
        double pawnPromotionWeight = interpolateOpeningEnding(maxPieceCount, 0d, weights[1]);
        double openingCentralityWeight = interpolateOpeningEnding(minPieceCount, weights[2], 0d);
        double endingCentralityWeight = interpolateOpeningEnding(minPieceCount, 0d, weights[3]);
        double centralityWeight = weights[4];
        double kingAttackWeight = weights[5];
        double kingSafetyWeight = weights[6];
        double castlingWeight = interpolateOpeningEnding(minPieceCount, weights[7], 0d);
        double pieceWeight = weights[8];
        //
        double value = pawnMultiplicityWeight * pawnMultiplicityValue
                     + pawnPromotionWeight * pawnPromotionValue
                     + openingCentralityWeight * pawnCentralityValue
                     + endingCentralityWeight * queenKingCentralityValue
                     + centralityWeight * knightBishopRookCentralityValue
                     + kingAttackWeight * kingAttackValue
                     + kingSafetyWeight * kingSafetyValue
                     + castlingWeight * castlings
                     + pieceWeight * pieceValue; // aus Sicht von Weiß
        /*
        var weightsVector = DoubleVector.fromArray(DoubleVector.SPECIES_256,
                                            new double[] { pawnPositionWeight,
                                                centralityWeight,
                                                kingProximityWeight,
                                                pieceWeight }, 0);
        var valuesVector = DoubleVector.fromArray(DoubleVector.SPECIES_256,
                                            new double[] { pawnPositionValue,
                                                centralityValue,
                                                kingProximityValue,
                                                pieceValue}, 0);
        double value = weightsVector.mul(valuesVector).reduceLanes(VectorOperators.ADD); // aus Sicht von Weiß
        */
        //
        if (color == Color.WHITE) return value;
        //
        return -value;
    }
    //
    private static double interpolateOpeningEnding(int pieceCount, double openingWeight, double endingWeight)
    {
        return pieceCount * openingWeight + (16 - pieceCount) * endingWeight;
    }
    //
    private static int eighthRankProximity(Position whitePawnPosition)
    {
        return square(whitePawnPosition.getY());
    }
    //
    private static int firstRankProximity(Position blackPawnPosition)
    {
        return square(7 - blackPawnPosition.getY());
    }
    //
    private static int centrality(Position position)
    {
        int x = position.getX();
        int y = position.getY();
        //
        if (x > 3) x = 7 - x;
        //
        if (y > 3) y = 7 - y;
        //
        return 18 - square(x - 3) - square(y - 3);
    }
    //
    private static int horizontalCentrality(Position position)
    {
        int x = position.getX();
        //
        if (x > 3) x = 7 - x;
        //
        return 9 - square(x - 3);
    }
    //
    private static int proximity(Position position1, Position position2)
    {
        return 98 - square(position1.getX() - position2.getX()) - square(position1.getY() - position2.getY());
    }
    //
    private static int rankDistance(Position position1, Position position2)
    {
        return Math.abs(position1.getY() - position2.getY());
    }
    //
    private static int fileDistance(Position position1, Position position2)
    {
        return Math.abs(position1.getX() - position2.getX());
    }
    //
    private static boolean isAdjacent(final int rankDistance, final int fileDistance)
    {
        return rankDistance + fileDistance <= 2
            && rankDistance < 2
            && fileDistance < 2;
    }
    //
    private static boolean isDifferentSquareColor(final int rankDistance, final int fileDistance)
    {
        return ((rankDistance + fileDistance) & 1) == 1;
    }
    //
    private static int square(final int r)
    {
        return r * r;
    }
    //
    private static int opponentRookKingProximity(Position position1, Position position2)
    {
        int rankDistance = rankDistance(position1, position2);
        int fileDistance = fileDistance(position1, position2);
        //
        if (isAdjacent(rankDistance, fileDistance)) return 0; // zu nah
        //
        return square(7 - Math.min(rankDistance, fileDistance));
    }
    //
    private static int opponentBishopKingProximity(Position position1, Position position2)
    {
        int rankDistance = rankDistance(position1, position2);
        int fileDistance = fileDistance(position1, position2);
        //
        if (isDifferentSquareColor(rankDistance, fileDistance)) return 0; // nicht erreichbar
        //
        if (isAdjacent(rankDistance, fileDistance)) return 0; // zu nah
        //
        return square(7 - Math.abs(rankDistance - fileDistance));
    }
    //
    private static int opponentKnightKingProximity(Position position1, Position position2)
    {
        int rankDistance = rankDistance(position1, position2);
        int fileDistance = fileDistance(position1, position2);
        //
        if (isAdjacent(rankDistance, fileDistance)) return 0; // zu nah
        //
        return square(7 - (rankDistance + fileDistance) / 3);
    }
    //
    private static int opponentQueenKingProximity(Position position1, Position position2)
    {
        int rankDistance = rankDistance(position1, position2);
        int fileDistance = fileDistance(position1, position2);
        //
        if (isAdjacent(rankDistance, fileDistance)) return 0; // zu nah
        //
        int diagonalDistance = Math.abs(rankDistance - fileDistance);
        //
        if (isDifferentSquareColor(rankDistance, fileDistance)) diagonalDistance++;
        //
        int horizontalDistance = Math.min(rankDistance, fileDistance);
        //
        return square(7 - Math.min(diagonalDistance, horizontalDistance));
    }
    //
    private static int opponentKingKingProximity(Position position1, Position position2)
    {
        int rankDistance = rankDistance(position1, position2);
        int fileDistance = fileDistance(position1, position2);
        //
        return square(7 - Math.max(rankDistance, fileDistance));
    }
    //
    private static int ownKingProximity(Position position1, Position position2)
    {
        return proximity(position1, position2);
    }
    //
    public int hashCode()
    {
        if (hash != 0) return hash;
        //
        int r0;
        int r1 = (color == Color.WHITE ? 1 : 0);
        int r2 = 0;
        //
        for (var piece : pieces)
        {
            if (piece == null) continue;
            //
            r0 = r1;
            r1 = r2;
            r2 = (31 * piece.hashCode() + 29 * r1 + 27 * r0);
        }
        //
        if (r2 == 0) r2 = 1;
        //
        if (isOpponentKingUnmoved()) // für Chess Database
        {
            if (isOpponentKingsideRookUnmoved()) r2 += 1;
            //
            if (isOpponentQueensideRookUnmoved()) r2 += 2;
        }
        //
        hash = r2;
        //
        return hash;
    }
    //
    public boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (o == null || getClass() != o.getClass()) return false;
        //
        var board = (Board) o;
        //
        return equalPosition(board) && equalMoves(board) && equalOpponentCastlingPermissions(board);
    }
    //
    public boolean equalPosition(Board board)
    {
        if (color != board.color) return false;
        //
        return Arrays.equals(pieces, board.pieces);
    }
    //
    private boolean equalMoves(Board board)
    {
        var moves1 = generateMoves();
        var moves2 = board.generateMoves();
        //
        if (moves1.size() != moves2.size()) return false;
        //
        var moveSet = new HashSet<Move>(moves1.size());
        //
        while (!moves1.isEmpty())
        {
            moveSet.add(moves1.poll());
        }
        //
        while (!moves2.isEmpty())
        {
            if (!moveSet.contains(moves2.poll())) return false;
        }
        //
        return true;
    }
    //
    private boolean equalOpponentCastlingPermissions(Board board)
    {
        boolean kingsidePermission1 = false;
        boolean queensidePermission1 = false;
        boolean kingsidePermission2 = false;
        boolean queensidePermission2 = false;
        //
        if (isOpponentKingUnmoved())
        {
            kingsidePermission1 = isOpponentKingsideRookUnmoved();
            queensidePermission1 = isOpponentQueensideRookUnmoved();
        }
        //
        if (board.isOpponentKingUnmoved())
        {
            kingsidePermission2 = board.isOpponentKingsideRookUnmoved();
            queensidePermission2 = board.isOpponentQueensideRookUnmoved();
        }
        //
        return kingsidePermission1 == kingsidePermission2 && queensidePermission1 == queensidePermission2;
    }
    //
    private King getOpponentKing()
    {
        return color == Color.BLACK ? whiteKing : blackKing;
    }
    //
    private boolean isOpponentKingUnmoved()
    {
        var king = getOpponentKing();
        //
        return king != null && !king.wasMoved();
    }
    //
    private boolean isOpponentKingsideRookUnmoved()
    {
        var piece = color == Color.WHITE ? getPieceAt(7, 7) : getPieceAt(7, 0);
        //
        return piece != null && !piece.wasMoved();
    }
    //
    private boolean isOpponentQueensideRookUnmoved()
    {
        var piece = color == Color.WHITE ? getPieceAt(0, 7) : getPieceAt(0, 0);
        //
        return piece != null && !piece.wasMoved();
    }
    //
    public String toString()
    {
        var builder = new StringBuilder();
        //
        for (var piece : pieces)
        {
            if (piece == null || piece.getColor() == Color.BLACK) continue;
            //
            builder.append(piece);
            builder.append(' ');
        }
        //
        builder.append("/");
        //
        for (var piece : pieces)
        {
            if (piece == null || piece.getColor() == Color.WHITE) continue;
            //
            builder.append(' ');
            builder.append(piece);
        }
        //
        return builder.toString();
    }
}
