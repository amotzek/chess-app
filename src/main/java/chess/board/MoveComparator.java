package chess.board;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.Comparator;
import chess.move.Move;
import chess.piece.King;
import chess.piece.Pawn;
import chess.piece.Piece;

/**
 * Created by andreasm 25.05.14 17:45
 */
final class MoveComparator implements Comparator<Move>
{
    private final Position position;
    private final Move nextMove;
    private final int[] defence;
    private final boolean[] attacksTo;
    //
    public MoveComparator(Move beforeMove, Move nextMove, int[] defence, boolean[] attacksTo)
    {
        super();
        //
        this.position = getDestination(beforeMove);
        this.nextMove = nextMove;
        this.defence = defence;
        this.attacksTo = attacksTo;
    }
    //
    public int compare(Move m1, Move m2)
    {
        if (m1 == m2) return 0;
        //
        if (nextMove != null)
        {
            // Wenn m1 der beste Zug ist, dann m1 > m2
            if (m1.equals(nextMove)) return -1;
            // Wenn m2 der beste Zug ist, dann m2 > m1
            if (m2.equals(nextMove)) return 1;
        }
        //
        boolean f1 = m1.getSecondPiece() instanceof King;
        boolean f2 = m2.getSecondPiece() instanceof King;
        //
        if (f1 && !f2) return -1; // m1 schlägt den König und m2 nicht, dann m1 > m2
        //
        if (!f1 && f2) return 1; // m2 schlägt den König und m1 nicht, dann m1 < m2
        //
        f1 = m1.isPromotion();
        f2 = m2.isPromotion();
        //
        if (f1 && !f2) return -1; // m1 ist eine Umwandlung und m2 nicht, dann m1 > m2
        //
        if (!f1 && f2) return 1; // m2 ist eine Umwandlung und m1 nicht, dann m1 < m2
        //
        var p1 = m1.getFirstPiece();
        var p2 = m2.getFirstPiece();
        var pp1 = p1.getPosition();
        var pp2 = p2.getPosition();
        int i1 = Board.XYtoFieldIndex(pp1.getX(), pp1.getY());
        int i2 = Board.XYtoFieldIndex(pp2.getX(), pp2.getY());
        //
        int v;
        int w;
        //
        if (m1.isCapture())
        {
            if (m2.isCapture())
            {
                // Bei beiden Zügen werden Figuren geschlagen
                if (position != null)
                {
                    f1 = pp1 == position;
                    f2 = pp2 == position;
                    //
                    if (f1 && !f2) return -1; // m1 schlägt die vorher gezogene Figur
                    //
                    if (!f1 && f2) return 1; // m2 schlägt die vorher gezogene Figur
                }
                //
                v = defence[i1];
                w = defence[i2];
                //
                if (v == 0)
                {
                    // Wenn das Zielfeld von m1 nicht gedeckt ist und das Zielfeld von m2 gedeckt ist, dann m1 < m2
                    if (w > 0) return 1;
                }
                else
                {
                    // Wenn das Zielfeld von m1 gedeckt ist und das Zielfeld von m2 nicht gedeckt ist, dann m1 > m2
                    if (w == 0) return -1;
                    // Wenn das Zielfeld von m1 von einer billigeren Figur gedeckt wird als das von m2, dann m1 > m2
                    if (v < w) return -1;
                    // Wenn das Zielfeld von m1 von einer wertvolleren Figur gedeckt wird als das von m2, dann m1 < m2
                    if (v > w) return 1;
                }
                //
                v = m1.getSecondPiece().getValue();
                w = m2.getSecondPiece().getValue();
                // Wenn bei m1 eine wertvollere Figur geschlagen wird als bei m2, dann m1 > m2
                if (v > w) return -1;
                // Wenn bei m2 eine wertvollere Figur geschlagen wird als bei m1, dann m1 < m2
                if (v < w) return 1;
                // m1 > m2, wenn bei m1 eine weniger wertvolle Figur zieht als bei m2
                return p1.getValue() - p2.getValue();
            }
            //
            return -1;
        }
        // Wenn bei m1 keine Figur geschlagen wird, aber bei m2, dann m1 < m2
        if (m2.isCapture()) return 1;
        //
        v = promotionProximity(p1, pp1);
        w = promotionProximity(p2, pp2);
        //
        if (v > w) return -1; // m1 ist näher an der Umwandlung
        //
        if (v < w) return 1; // m2 ist näher an der Umwandlung
        //
        f1 = attacksTo[i1];
        f2 = attacksTo[i2];
        //
        if (!f1 && f2) return -1; // Wenn das Zielfeld von m1 unbedroht ist und das Zielfeld von m2 bedroht, wähle m1
        //
        if (f1 && !f2) return 1; // Wenn das Zielfeld von m1 bedroht ist und das Zielfeld von m2 unbedroht, wähle m2
        //
        v = defence[i1];
        w = defence[i2];
        //
        if (v == 0)
        {
            if (w > 0) return 1;
        }
        else
        {
            if (w == 0) return -1;
            //
            if (v < w) return -1;
            //
            if (v > w) return 1;
        }
        //
        f1 = p1 instanceof Pawn;
        f2 = p2 instanceof Pawn;
        //
        if (!f1 && f2) return -1; // m1 zieht einen Offizier oder einen König, m2 nicht
        //
        if (f1 && !f2) return 1;  // m2 zieht einen Offizier oder einen König, m1 nicht
        //
        int d = p1.getValue() - p2.getValue();
        //
        if (d == 0 && position != null) d = destinationDistance(position, m1) - destinationDistance(position, m2);
        //
        return d;
    }
    //
    private static Position getDestination(Move move)
    {
        if (move == null) return null;
        //
        var piece = move.getFirstPiece();
        //
        if (piece == null) return null; // Null Move
        //
        return piece.getPosition();
    }
    //
    private static int destinationDistance(Position position, Move move)
    {
        var destination = getDestination(move);
        //
        return Math.abs(position.getX() - destination.getX()) + Math.abs(position.getY() - destination.getY());
    }
    //
    private static int promotionProximity(Piece p, Position pp)
    {
        if (p instanceof Pawn)
        {
            int v = (p.getColor() == Color.WHITE) ? pp.getY() : 8 - pp.getY();
            //
            if (v < 5) return 0;
            //
            return v;
        }
        //
        return 0;
    }
}
