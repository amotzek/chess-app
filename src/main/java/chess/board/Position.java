package chess.board;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
public final class Position
{
    private final int x, y;
    //
    private Position(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    //
    private static final Position[][] positions = new Position[8][8];
    //
    static
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                positions[x][y] = new Position(x, y);
            }
        }
    }
    //
    public static Position getInstance(int x, int y)
    {
        if (isOutOfBounds(x, y)) throw new IllegalArgumentException();
        //
        return positions[x][y];
    }
    //
    public static Position getInstance(char h, int v)
    {
        return getInstance(h - 'a', v - 1);
    }
    //
    public static boolean isOutOfBounds(int x, int y)
    {
        return (x < 0 || x > 7 || y < 0 || y > 7);
    }
    //
    public int getX()
    {
        return x;
    }
    //
    public int getY()
    {
        return y;
    }
    //
    public boolean is(int x, int y)
    {
        return (this.x == x) && (this.y == y);
    }
    //
    public Color squareColor()
    {
        return squareColor(x, y);
    }
    //
    public static Color squareColor(int x, int y)
    {
        if (((x + y) & 1) == 0) return Color.BLACK;
        //
        return Color.WHITE;
    }
    //
    public int hashCode()
    {
        return x + 8 * y;
    }
    //
    public boolean equals(Object o)
    {
        return this == o;
    }
    //
    public String toString()
    {
        var builder = new StringBuilder();
        builder.append((char) ('a' + x));
        builder.append(y + 1);
        //
        return builder.toString();
    }
}