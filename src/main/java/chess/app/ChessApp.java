package chess.app;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.Executors;
import chess.board.Board;
import chess.controller.BoardCanvasController;
import chess.controller.BoardFrameController;
import chess.controller.BoardFrameMovementController;
import chess.model.BoardModel;
import chess.model.GameModel;
import chess.model.MoveModel;
import chess.view.BoardCanvas;
import chess.view.BoardFrame;
import chess.view.MoveChoicePopup;
/*
 * Created by andreasm on 27.05.2014.
 */
public final class ChessApp
{
    public static void main(String[] args)
    {
        var executor = Executors.newCachedThreadPool();
        //
        var board = new Board();
        var boardModel = new BoardModel(board);
        var gameModel = new GameModel();
        var moveModel = new MoveModel();
        //
        var boardCanvas = new BoardCanvas(gameModel, boardModel);
        var boardFrame = new BoardFrame(boardCanvas);
        var choicePopup = new MoveChoicePopup(executor, boardModel, gameModel, moveModel);
        var loggerAdapter = new LoggerAdapter();
        //
        var boardCanvasController = new BoardCanvasController(executor, boardFrame, choicePopup, boardModel, gameModel, moveModel);
        var frameController = new BoardFrameController();
        var movementController = new BoardFrameMovementController(boardFrame);
        //
        gameModel.addListener(boardFrame);
        gameModel.addListener(boardCanvas);
        gameModel.addListener(loggerAdapter);
        boardModel.addListener(boardCanvas);
        //
        boardCanvas.addMouseListener(boardCanvasController);
        boardCanvas.addMouseMotionListener(movementController);
        boardFrame.addWindowListener(frameController);
        //
        boardCanvas.add(choicePopup);
        //
        boardFrame.setLocationRelativeTo(null);
        boardFrame.setVisible(true);
    }
}