package chess.app;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.logging.Logger;
import chess.board.Color;
import chess.model.GameModelListener;
import chess.move.Move;
import chess.piece.Piece;
/*
 * Created by andreasm on 28.05.2014.
 */
final class LoggerAdapter implements GameModelListener
{
    private final Logger logger;
    //
    public LoggerAdapter()
    {
        logger = Logger.getLogger("Chess");
    }
    //
    public void playerChanged(Color playerColor, Color humanColor)
    {
        logger.info(playerColor.toString());
    }
    //
    public void pieceCleared()
    {
    }
    //
    public void pieceTaken(Piece piece)
    {
    }
    //
    public void piecePut(Move move)
    {
        logger.info(move.toString());
    }
    //
    public void info(String message)
    {
        logger.info(message);
    }
}