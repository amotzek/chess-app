package chess.piece;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import chess.board.Color;
import chess.board.Position;
import chess.move.MoveQueue;
//
public abstract class Piece
{
    protected final Color color;
    protected final Position position;
    protected final Piece before;
    protected final int index;
    //
    public Piece(char h, int v, Color color, int index)
    {
        this.color = color;
        this.index = index;
        position = Position.getInstance(h, v);
        before = null;
    }
    //
    protected Piece(Piece before, Position position)
    {
        this.position = position;
        this.before = before;
        color = before.color;
        index = before.index;
    }
    //
    public final Color getColor()
    {
        return color;
    }
    //
    public final Position getPosition()
    {
        return position;
    }
    //
    public final Piece getBefore() { return before; }
    //
    public final int getIndex()
    {
        return index;
    }
    //
    public final boolean wasMoved()
    {
        return before != null;
    }
    //
    public final boolean wasCaptured()
    {
        return position == null;
    }
    //
    protected final boolean addAttack(Board board, int dx, int dy)
    {
        final int x = position.getX() + dx;
        final int y = position.getY() + dy;
        //
        if (Position.isOutOfBounds(x, y)) return false;
        //
        return board.addAttack(this, x, y);
    }
    //
    public abstract void addAttacks(Board board);
    //
    public void generateMoves(Board board, MoveQueue moves)
    {
        if (color != board.getColor()) return;
        //
        if (wasCaptured()) return;
        //
        var attacksTo = board.getAttacksFrom(this);
        //
        if (attacksTo == null) return;
        //
        for (var position : attacksTo)
        {
            int x = position.getX();
            int y = position.getY();
            //
            if (canMoveToAttackedSquare() || !board.isAttacked(x, y))
            {
                if (canMoveToEmptySquare() && board.isEmpty(x, y))
                {
                    moves.offerMove(move(x, y));
                }
                else if (board.canCapture(x, y))
                {
                    var piece = board.getPieceAt(x, y).capture();
                    moves.offerCapture(move(x, y), piece);
                }
            }
        }
    }
    //
    public boolean canMoveToAttackedSquare()
    {
        return true;
    }
    //
    public boolean canMoveToEmptySquare()
    {
        return true;
    }
    //
    public abstract Piece move(int x, int y);
    //
    public abstract Piece capture();
    //
    public abstract String getAbbreviatedName();
    //
    public abstract int getValue();
    //
    public final int hashCode()
    {
        int hash = 64 * getValue();
        //
        if (position != null) hash += position.hashCode();
        //
        hash <<= 1;
        //
        if (color == Color.WHITE) hash++;
        //
        return hash;
    }
    //
    public final boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (o == null || getClass() != o.getClass()) return false;
        //
        var piece = (Piece) o;
        //
        if (color != piece.color) return false;
        //
        if (position != null)
        {
            if (piece.position == null) return false;
            //
            return position.equals(piece.position);
        }
        //
        return piece.position == null;
    }
    //
    public final String toString()
    {
        var builder = new StringBuilder();
        builder.append(getAbbreviatedName());
        builder.append(position.toString());
        //
        return builder.toString();
    }
}