package chess.piece;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import chess.board.Color;
import chess.board.Position;
import chess.move.MoveQueue;
//
public final class King extends Piece
{
    public King(char h, int v, Color color, int index)
    {
        super(h, v, color, index);
    }
    //
    private King(Piece piece, Position position)
    {
        super(piece, position);
    }
    //
    public Piece move(int x, int y)
    {
        return new King(this, Position.getInstance(x, y));
    }
    //
    public Piece capture()
    {
        return new King(this, null);
    }
    //
    public void addAttacks(Board board)
    {
        if (wasCaptured()) return;
        //
        addAttack(board, -1, -1);
        addAttack(board, -1, 0);
        addAttack(board, -1, 1);
        addAttack(board, 0, -1);
        addAttack(board, 0, 1);
        addAttack(board, 1, -1);
        addAttack(board, 1, 0);
        addAttack(board, 1, 1);
    }
    //
    public boolean canMoveToAttackedSquare()
    {
        return false;
    }
    //
    public void generateMoves(Board board, MoveQueue moves)
    {
        if (color != board.getColor()) return;
        //
        super.generateMoves(board, moves);
        //
        if (wasMoved()) return;
        //
        final int y = (color == Color.WHITE) ? 0 : 7;
        //
        {
            // große Rochade
            var piece = board.getPieceAt(0, y);
            //
            if (piece != null
            && !piece.wasMoved()
            && board.isEmpty(1, y)
            && board.isEmpty(2, y)
            && board.isEmpty(3, y)
            && !board.isAttacked(4, y)
            && !board.isAttacked(3, y)
            && !board.isAttacked(2, y))
            {
                var king = (King) move(2, y);
                var rook = (Rook) piece.move(3, y);
                moves.offerCastling(king, rook);
            }
        }
        {
            // kleine Rochade
            var piece = board.getPieceAt(7, y);
            //
            if (piece != null
            && !piece.wasMoved()
            && board.isEmpty(6, y)
            && board.isEmpty(5, y)
            && !board.isAttacked(4, y)
            && !board.isAttacked(5, y)
            && !board.isAttacked(6, y))
            {
                var king = (King) move(6, y);
                var rook = (Rook) piece.move(5, y);
                moves.offerCastling(king, rook);
            }
        }
    }
    //
    public String getAbbreviatedName()
    {
        return "K";
    }
    //
    public int getValue()
    {
        return 330;
    }
}