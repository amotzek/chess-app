package chess.piece;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import chess.board.Color;
import chess.board.Position;
//
public final class Rook extends Piece
{
    public Rook(char h, int v, Color color, int index)
    {
        super(h, v, color, index);
    }
    //
    private Rook(Piece piece, Position position)
    {
        super(piece, position);
    }
    //
    public Piece move(int x, int y)
    {
        return new Rook(this, Position.getInstance(x, y));
    }
    //
    public Piece capture()
    {
        return new Rook(this, null);
    }
    //
    public void addAttacks(Board board)
    {
        if (wasCaptured()) return;
        //
        int d = -1;
        //
        while (addAttack(board, d, 0))
        {
            d--;
        }
        //
        d = 1;
        //
        while (addAttack(board, d, 0))
        {
            d++;
        }
        //
        d = -1;
        //
        while (addAttack(board, 0, d))
        {
            d--;
        }
        //
        d = 1;
        //
        while (addAttack(board, 0, d))
        {
            d++;
        }
    }
    //
    public String getAbbreviatedName()
    {
        return "T";
    }
    //
    public int getValue()
    {
        return 15;
    }
}