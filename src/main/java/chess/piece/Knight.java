package chess.piece;
//
import chess.board.Board;
import chess.board.Color;
import chess.board.Position;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
public final class Knight extends Piece
{
    public Knight(char h, int v, Color color, int index)
    {
        super(h, v, color, index);
    }
    //
    public Knight(Piece piece, Position position)
    {
        super(piece, position);
    }
    //
    public Piece move(int x, int y)
    {
        return new Knight(this, Position.getInstance(x, y));
    }
    //
    public Piece capture()
    {
        return new Knight(this, null);
    }
    //
    public void addAttacks(final Board board)
    {
        if (wasCaptured()) return;
        //
        addAttack(board, -2, 1);
        addAttack(board, -2, -1);
        addAttack(board, -1, 2);
        addAttack(board, -1, -2);
        addAttack(board, 1, -2);
        addAttack(board, 1, 2);
        addAttack(board, 2, -1);
        addAttack(board, 2, 1);
    }
    //
    public String getAbbreviatedName()
    {
        return "S";
    }
    //
    public int getValue()
    {
        return 8;
    }
}