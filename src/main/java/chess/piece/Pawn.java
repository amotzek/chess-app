package chess.piece;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import chess.board.Color;
import chess.board.Position;
import chess.move.MoveQueue;
import chess.move.UnorderedMoves;
//
public final class Pawn extends Piece
{
    public Pawn(char h, int v, Color color, int index)
    {
        super(h, v, color, index);
    }
    //
    private Pawn(Piece piece, Position position)
    {
        super(piece, position);
    }
    //
    public Piece move(int x, int y)
    {
        return new Pawn(this, Position.getInstance(x, y));
    }
    //
    public Piece capture()
    {
        return new Pawn(this, null);
    }
    //
    public void addAttacks(Board board)
    {
        if (wasCaptured()) return;
        //
        if (color == Color.WHITE)
        {
            addAttack(board, -1, 1);
            addAttack(board, 1, 1);
        }
        else
        {
            addAttack(board, -1, -1);
            addAttack(board, 1, -1);
        }
    }
    //
    public void generateMoves(Board board, MoveQueue moves)
    {
        if (color != board.getColor()) return;
        //
        if (wasCaptured()) return;
        //
        var attackMoves = new UnorderedMoves();
        super.generateMoves(board, attackMoves);
        //
        while (!attackMoves.isEmpty())
        {
            var move = attackMoves.poll();
            var pawn = (Pawn) move.getFirstPiece();
            var attacked = move.getSecondPiece();
            //
            if (pawn.mustPromote())
            {
                var position = pawn.getPosition();
                var queen = new Queen(this, position);
                var knight = new Knight(this, position);
                moves.offerPromotionAndCapture(queen, attacked);
                moves.offerPromotionAndCapture(knight, attacked);
            }
            else
            {
                moves.offerCapture(pawn, attacked);
            }
        }
        //
        int x = position.getX();
        int y = position.getY();
        int dy = (color == Color.WHITE ? 1 : -1);
        //
        if (board.getPieceAt(x, y + dy) == null)
        {
            var pawn1 = (Pawn) move(x, y + dy);
            //
            if (pawn1.mustPromote())
            {
                var position = pawn1.getPosition();
                var queen = new Queen(this, position);
                var knight = new Knight(this, position);
                moves.offerPromotion(queen);
                moves.offerPromotion(knight);
            }
            else
            {
                moves.offerMove(pawn1);
            }
            //
            if (canMoveTwoRanks() && board.getPieceAt(x, y + 2 * dy) == null)
            {
                Piece pawn2 = move(x, y + 2 * dy);
                moves.offerMove(pawn2);
            }
        }
        //
        var move = board.getBeforeMove();
        //
        if (move == null) return;
        //
        var pawn3 = move.getFirstPiece();
        //
        if (pawn3 instanceof Pawn)
        {
            /*
             * En Passant kann nur unmittelbar nach einem Doppelschritt
             * eines gegnerischen Bauern auftreten. Später verfällt das Recht.
             * Der gegnerische Bauer steht vor dem En Passant neben dem Eigenen.
             * Der eigene Bauer schlägt den Gegnerischen, als hätte dieser
             * einen Einzelschritt getan.
             */
            var position3 = pawn3.getPosition();
            int y3 = position3.getY();
            //
            if (y3 == y) // selber Rang
            {
                var pawn4 = pawn3.getBefore();
                var position4 = pawn4.getPosition();
                //
                if (Math.abs(position3.getY() - position4.getY()) == 2) // Doppelschritt zuvor
                {
                    int x3 = position3.getX();
                    //
                    if (Math.abs(x - x3) == 1) // unmittelbar neben
                    {
                        moves.offerEnPassant((Pawn) move(x3, y + dy), (Pawn) pawn3.capture());
                    }
                }
            }
        }
    }
    //
    public boolean canMoveToEmptySquare()
    {
        return false;
    }
    //
    private boolean mustPromote()
    {
        if (position == null) return false;
        //
        if (color == Color.WHITE) return position.getY() == 7;
        //
        return position.getY() == 0;
    }
    //
    private boolean canMoveTwoRanks()
    {
        return before == null;
    }
    //
    public String getAbbreviatedName()
    {
        return "";
    }
    //
    public int getValue()
    {
        return 2;
    }
}