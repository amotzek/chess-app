package chess.piece;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import chess.board.Color;
import chess.board.Position;
//
public final class Bishop extends Piece
{
    public Bishop(char h, int v, Color color, int index)
    {
        super(h, v, color, index);
    }
    //
    private Bishop(Piece before, Position position)
    {
        super(before, position);
    }
    //
    public Piece move(int x, int y)
    {
        return new Bishop(this, Position.getInstance(x, y));
    }
    //
    public Piece capture()
    {
        return new Bishop(this, null);
    }
    //
    public void addAttacks(Board board)
    {
        if (wasCaptured()) return;
        //
        int dx, dy;
        dx = -1;
        dy = -1;
        //
        while (addAttack(board, dx, dy))
        {
            dx--;
            dy--;
        }
        //
        dx = -1;
        dy = 1;
        //
        while (addAttack(board, dx, dy))
        {
            dx--;
            dy++;
        }
        //
        dx = 1;
        dy = -1;
        //
        while (addAttack(board, dx, dy))
        {
            dx++;
            dy--;
        }
        //
        dx = 1;
        dy = 1;
        //
        while (addAttack(board, dx, dy))
        {
            dx++;
            dy++;
        }
    }
    //
    public String getAbbreviatedName()
    {
        return "L";
    }
    //
    public int getValue()
    {
        return 11;
    }
}