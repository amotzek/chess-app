package chess.piece;
/*
 * Copyright (C) 1998, 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import chess.board.Board;
import chess.board.Color;
import chess.board.Position;
//
public final class Queen extends Piece
{
    public Queen(char h, int v, Color color, int index)
    {
        super(h, v, color, index);
    }
    //
    public Queen(Piece piece, Position position)
    {
        super(piece, position);
    }
    //
    public Piece move(int x, int y)
    {
        return new Queen(this, Position.getInstance(x, y));
    }
    //
    public Piece capture()
    {
        return new Queen(this, null);
    }
    //
    public void addAttacks(Board board)
    {
        if (wasCaptured()) return;
        //
        int dx = -1;
        int dy = -1;
        //
        while (addAttack(board, dx, dy))
        {
            dx--;
            dy--;
        }
        //
        dx = -1;
        dy = 1;
        //
        while (addAttack(board, dx, dy))
        {
            dx--;
            dy++;
        }
        //
        dx = 1;
        dy = -1;
        //
        while (addAttack(board, dx, dy))
        {
            dx++;
            dy--;
        }
        //
        dx = 1;
        dy = 1;
        //
        while (addAttack(board, dx, dy))
        {
            dx++;
            dy++;
        }
        //
        dx = -1;
        //
        while (addAttack(board, dx, 0))
        {
            dx--;
        }
        //
        dx = 1;
        //
        while (addAttack(board, dx, 0))
        {
            dx++;
        }
        //
        dy = -1;
        //
        while (addAttack(board, 0, dy))
        {
            dy--;
        }
        //
        dy = 1;
        //
        while (addAttack(board, 0, dy))
        {
            dy++;
        }
    }
    //
    public String getAbbreviatedName()
    {
        return "D";
    }
    //
    public int getValue()
    {
        return 29;
    }
}