package chess.controller;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import chess.view.BoardFrame;
/*
 * Created by andreasm on 27.05.2014.
 */
public final class BoardFrameController implements WindowListener
{
    public void windowClosing(WindowEvent e)
    {
        var frame = (BoardFrame) e.getWindow();
        frame.dispose();
    }
    //
    public void windowOpened(WindowEvent e)
    {
        // ignore
    }
    //
    public void windowClosed(WindowEvent e)
    {
        // ignore
    }
    //
    public void windowIconified(WindowEvent e)
    {
        // ignore
    }
    //
    public void windowDeiconified(WindowEvent e)
    {
        // ignore
    }
    //
    public void windowActivated(WindowEvent e)
    {
        // ignore
    }
    //
    public void windowDeactivated(WindowEvent e)
    {
        // ignore
    }
}