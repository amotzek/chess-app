package chess.controller;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import chess.board.Position;
import chess.model.BoardModel;
import chess.model.GameModel;
import chess.model.MoveModel;
import chess.move.Move;
import chess.piece.Piece;
import chess.view.BoardCanvas;
import chess.view.BoardFrame;
import chess.view.MoveChoicePopup;
/*
 * Created by andreasm 27.05.14 19:13
 */
public final class BoardCanvasController implements MouseListener
{
    private final ExecutorService executor;
    private final BoardFrame boardFrame;
    private final MoveChoicePopup choicePopup;
    private final BoardModel boardModel;
    private final GameModel gameModel;
    private final MoveModel moveModel;
    //
    public BoardCanvasController(ExecutorService executor, BoardFrame boardFrame, MoveChoicePopup choicePopup, BoardModel boardModel, GameModel gameModel, MoveModel moveModel)
    {
        this.executor = executor;
        this.boardFrame = boardFrame;
        this.choicePopup = choicePopup;
        this.boardModel = boardModel;
        this.gameModel = gameModel;
        this.moveModel = moveModel;
    }
    //
    public void mouseReleased(MouseEvent e)
    {
        int x = e.getX();
        int y = e.getY();
        //
        if (x >= BoardCanvas.CROSS_X && x <= (BoardCanvas.CROSS_X + BoardCanvas.CROSS_SIDE) && y >= BoardCanvas.CROSS_Y && y <= (BoardCanvas.CROSS_Y + BoardCanvas.CROSS_SIDE))
        {
            boardFrame.dispose();
            executor.shutdown();
            //
            return;
        }
        //
        if (x >= BoardCanvas.ARROW_X && x <= (BoardCanvas.ARROW_X + BoardCanvas.ARROW_SIDE) && y >= BoardCanvas.ARROW_Y && y <= (BoardCanvas.ARROW_Y + BoardCanvas.ARROW_SIDE))
        {
            if (boardModel.isBeforeFirstMove() && gameModel.playsHumanWhite() && !gameModel.hasPiece())
            {
                gameModel.setHumanColorBlack();
                var engine = new ChessEngine(boardModel, gameModel);
                executor.execute(engine);
            }
            //
            return;
        }
        //
        if (gameModel.getPlayerColor() == gameModel.getHumanColor())
        {
            var boardCanvas = (BoardCanvas) e.getComponent();
            var position = boardCanvas.coordinatesToPosition(x, y);
            //
            if (position == null) return;
            //
            if (gameModel.hasPiece())
            {
                var piece = gameModel.getPiece();
                var moves = findMoves(piece, position);
                //
                if (moves.size() == 1)
                {
                    var move = moves.get(0);
                    var command = new MakeMoveCommand(boardModel, gameModel, move, executor);
                    command.run();
                }
                else if (moves.size() > 1)
                {
                    moveModel.setMoves(moves);
                    choicePopup.show(boardCanvas, x, y);
                }
            }
            else
            {
                var piece = findPiece(position);
                //
                if (piece != null) gameModel.takePiece(piece);
            }
        }
    }
    //
    public void mouseExited(MouseEvent e)
    {
        if (gameModel.getPlayerColor() == gameModel.getHumanColor()) gameModel.clearPiece();
    }
    //
    public void mouseClicked(MouseEvent e)
    {
        // ignore
    }
    //
    public void mousePressed(MouseEvent e)
    {
        // ignore
    }
    //
    public void mouseEntered(MouseEvent e)
    {
        // ignore
    }
    //
    private List<Move> findMoves(Piece piece1, Position position1)
    {
        var moves = boardModel.getMoves();
        var matchingMoves = new LinkedList<Move>();
        //
        while (!moves.isEmpty())
        {
            var move = moves.poll();
            var piece2 = move.getFirstPiece();
            var position2 = piece2.getPosition();
            //
            if (piece1 == piece2.getBefore() && position1.equals(position2)) matchingMoves.addLast(move);
        }
        //
        return matchingMoves;
    }
    //
    private Piece findPiece(Position position)
    {
        int x = position.getX();
        int y = position.getY();
        var piece = boardModel.getPieceAt(x, y);
        //
        if (piece != null && piece.getColor() == gameModel.getHumanColor()) return piece;
        //
        return null;
    }
}