package chess.controller;
/*
 * Copyright (C) 2021, 2022 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.text.DecimalFormat;
import chess.board.Board;
import chess.model.BoardModel;
import chess.model.GameModel;
import chess.search.ChessMiniMaxSearch;
import chess.search.opening.OpeningBook;
import chess.search.opening.OpeningService;
/*
 * Created by andreasm 27.05.14 20:49
 */
public final class ChessEngine implements Runnable
{
    private static final OpeningService openingService = new OpeningService();
    private static final OpeningBook openingBook = new OpeningBook();
    //
    private final BoardModel boardModel;
    private final GameModel gameModel;
    //
    public ChessEngine(BoardModel boardModel, GameModel gameModel)
    {
        this.boardModel = boardModel;
        this.gameModel = gameModel;
    }
    //
    public void run()
    {
        var board = boardModel.getBoard();
        var bookBoard = openingService.lookup(board);
        //
        if (bookBoard == null) bookBoard = openingBook.lookup(board);
        //
        if (bookBoard != null)
        {
            try
            {
                Thread.sleep(1000L);
            }
            catch (InterruptedException e)
            {
                // do nothing
            }
            //
            boardModel.setBoard(bookBoard);
            gameModel.setBoard(bookBoard);
            //
            return;
        }
        //
        var search = new ChessMiniMaxSearch(board, Board.STANDARD_PARAMETERS);
        long then = System.currentTimeMillis();
        var path = search.findBestPath(60000000L);
        long now = System.currentTimeMillis();
        double branchingFactor = search.getAverageBranchingFactor();
        long leafCount = search.getLeafCount();
        long performance = (1000L * leafCount) / (1L + now - then);
        long transpositionCount = search.getTranspositionCount();
        var decimalFormat = new DecimalFormat("#0.00");
        var messageBuilder = new StringBuilder();
        messageBuilder.append(decimalFormat.format(branchingFactor));
        messageBuilder.append(' ');
        messageBuilder.append(performance);
        messageBuilder.append(' ');
        messageBuilder.append(transpositionCount);
        //
        for (var element : path)
        {
            var move = element.getBeforeMove();
            messageBuilder.append(" ");
            messageBuilder.append(move);
        }
        //
        if (path.size() > 1)
        {
            board = path.get(1);
            board.setComment(messageBuilder.toString());
            boardModel.setBoard(board);
            gameModel.setBoard(board);
        }
    }
}