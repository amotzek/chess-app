package chess.controller;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.Executor;
import chess.model.BoardModel;
import chess.model.GameModel;
import chess.move.Move;
/*
 * Created by andreasm on 03.07.2014.
 */
public class MakeMoveCommand implements Runnable
{
    private final BoardModel boardModel;
    private final GameModel gameModel;
    private final Move move;
    private final Executor executor;
    //
    public MakeMoveCommand(BoardModel boardModel, GameModel gameModel, Move move, Executor executor)
    {
        this.boardModel = boardModel;
        this.gameModel = gameModel;
        this.move = move;
        this.executor = executor;
    }
    //
    public final void run()
    {
        boardModel.doMove(move);
        gameModel.putPiece(move);
        var engine = new ChessEngine(boardModel, gameModel);
        executor.execute(engine);
    }
}