package chess.controller;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import chess.view.BoardFrame;
/*
 * Created by andreasm on 28.05.2014.
 */
public final class BoardFrameMovementController implements MouseMotionListener
{
    private final BoardFrame boardFrame;
    private Point firstPoint;
    private Point firstLocation;
    //
    public BoardFrameMovementController(BoardFrame boardFrame)
    {
        this.boardFrame = boardFrame;
    }
    //
    public void mouseDragged(MouseEvent e)
    {
        if (firstPoint == null)
        {
            firstPoint = e.getLocationOnScreen();
            firstLocation = boardFrame.getLocation();
        }
        else
        {
            var currentPoint = e.getLocationOnScreen();
            int dx = currentPoint.x - firstPoint.x;
            int dy = currentPoint.y - firstPoint.y;
            //
            if (dx != 0 || dy != 0)
            {
                var location = new Point(firstLocation);
                location.translate(dx, dy);
                boardFrame.setLocation(location);
            }
            //
            e.consume();
        }
    }
    //
    public void mouseMoved(MouseEvent e)
    {
        firstPoint = null;
        firstLocation = null;
    }
}