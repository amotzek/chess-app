package ai.search.tree.minimax;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicLong;
import ai.search.tree.TreeSearch;
/*
 * Created by andreasm on 26.05.2014.
 */
public abstract class MiniMaxSearch<S> extends TreeSearch<S>
{
    public static final double WON = Integer.MAX_VALUE;
    public static final double LOST = -Integer.MAX_VALUE;
    public static final double DRAW = 0d;
    //
    private final AtomicLong branches;
    private final AtomicLong forks;
    private final AtomicLong leaves;
    private final AtomicLong transpositions;
    //
    protected MiniMaxSearch()
    {
        branches = new AtomicLong();
        forks = new AtomicLong();
        leaves = new AtomicLong();
        transpositions = new AtomicLong();
    }
    //
    protected abstract int getStartDepth();
    //
    protected abstract int getEndDepth();
    //
    protected abstract boolean isQuiesce(int depth, S state);
    //
    protected abstract double getAspirationWindowSize(int fails);
    //
    protected abstract void onIntermediatePath(List<S> path);
    //
    public final List<S> findBestPath(long tries)
    {
        var remaining = new AtomicLong(tries);
        int depth = getStartDepth();
        int endDepth = getEndDepth();
        var start = getStart();
        var startNode = new TreeNode<>(start);
        LinkedList<S> path = null;
        Double value = null;
        //
        try
        {
            while (depth <= endDepth) // iterative deepening
            {
                value = search(startNode, value, remaining, path != null, depth);
                path = extractPath(startNode);
                onIntermediatePath(Collections.unmodifiableList(path));
                //
                if (path.size() <= depth) break;
                //
                depth++;
            }
        }
        catch (LimitExceededException e)
        {
            // do nothing
        }
        //
        return path;
    }
    //
    private double search(TreeNode<S> startNode, Double value, AtomicLong remaining, boolean enforceLimit, int depth)
    {
        var table = new TranspositionTable<S>(depth);
        //
        if (value == null)
        {
            var action = new MiniMaxAction(depth, startNode, -Double.MAX_VALUE, Double.MAX_VALUE, remaining, enforceLimit, table);
            //
            return action.invoke();
        }
        //
        int failsLow = 0;
        int failsHigh = 0;
        double max = getAspirationWindowSize(0);
        double min = -max;
        max += value;
        min += value;
        //
        while (true)
        {
            var action = new MiniMaxAction(depth, startNode, min, max, remaining, enforceLimit, table);
            double intermediateValue = action.invoke();
            //
            if (intermediateValue <= min)
            {
                min = value - getAspirationWindowSize(++failsLow);
            }
            else if (intermediateValue >= max)
            {
                max = value + getAspirationWindowSize(++failsHigh);
            }
            else
            {
                return intermediateValue;
            }
        }
    }
    //
    private LinkedList<S> extractPath(TreeNode<S> node)
    {
        var path = new LinkedList<S>();
        //
        do
        {
            var state = node.getState();
            path.addLast(state);
            node = node.getNext();
        }
        while (node != null);
        //
        return path;
    }
    //
    private final class MiniMaxAction extends RecursiveTask<Double>
    {
        private final int depth;
        private final TreeNode<S> parentnode;
        private double maxValue;
        private double alpha;
        private final double beta;
        private final AtomicLong remaining;
        private final boolean enforceLimit;
        private final TranspositionTable<S> table;
        //
        private MiniMaxAction(int depth, TreeNode<S> parentnode, double alpha, double beta, AtomicLong remaining, boolean enforceLimit, TranspositionTable<S> table)
        {
            this.depth = depth;
            this.parentnode = parentnode;
            this.maxValue = -Double.MAX_VALUE;
            this.alpha = alpha;
            this.beta = beta;
            this.remaining = remaining;
            this.enforceLimit = enforceLimit;
            this.table = table;
        }
        //
        protected Double compute()
        {
            branches.incrementAndGet(); // Zweig
            var parent = parentnode.getState();
            var entry = table.getValue(depth, parent);
            //
            if (entry != null)
            {
                leaves.incrementAndGet(); // Blatt
                transpositions.incrementAndGet(); // Transposition
                parentnode.setNext(entry.getNext());
                //
                return entry.getValue();
            }
            //
            if (remaining.decrementAndGet() < 0 && enforceLimit) throw new LimitExceededException();
            //
            if (depth <= 0 && isQuiesce(depth, parent))
            {
                leaves.incrementAndGet();
                //
                return getValue(depth, parent);
            }
            //
            var childs = findSuccessors(parent);
            //
            if (childs.length == 0)
            {
                leaves.incrementAndGet();
                //
                return getValue(depth, parent);
            }
            //
            forks.incrementAndGet(); // Verzweigung
            //
            double originalAlpha = alpha;
            int index = 0;
            computeAction(createAction(childs[index++]));
            //
            while (index < childs.length && alpha < beta)
            {
                var action1 = createAction(childs[index++]);
                var action2 = index < childs.length ? createAction(childs[index++]) : null;
                var action3 = index < childs.length ? createAction(childs[index++]) : null;
                forkAction(action2);
                forkAction(action3);
                computeAction(action1);
                joinAction(action2);
                joinAction(action3);
            }
            //
            if (originalAlpha < maxValue && maxValue < beta) table.putValue(depth, parent, maxValue, parentnode.getNext());
            //
            return maxValue;
        }
        //
        private MiniMaxAction createAction(S child)
        {
            var childnode = new TreeNode<>(child);
            //
            return new MiniMaxAction(depth - 1, childnode, -beta, -alpha, remaining, enforceLimit, table);
        }
        //
        private void computeAction(MiniMaxAction action)
        {
            processValue(action, action.compute());
        }
        //
        private void forkAction(MiniMaxAction action)
        {
            if (action != null) action.fork();
        }
        //
        private void joinAction(MiniMaxAction action)
        {
            if (action != null) processValue(action, action.join());
        }
        //
        private void processValue(MiniMaxAction action, double value)
        {
            value = -value;
            //
            if (value > maxValue)
            {
                maxValue = value;
                parentnode.setNext(action.parentnode);
            }
            //
            if (maxValue > alpha)
            {
                alpha = maxValue;
            }
        }
    }
    //
    public final double getAverageBranchingFactor()
    {
        long divisor = forks.get();
        long dividend = branches.get();
        //
        if (divisor == 0) return 0d;
        //
        return ((double) dividend) / (double) divisor;
    }
    //
    public final long getLeafCount()
    {
        return leaves.get();
    }
    //
    public final long getTranspositionCount()
    {
        return transpositions.get();
    }
}