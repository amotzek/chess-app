package ai.search.tree.minimax;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.ConcurrentHashMap;
/*
 * Created by andreasm on 06.06.2014.
 */
final class TranspositionTable<S>
{
    private final int startDepth;
    private final ConcurrentHashMap<S, TranspositionTableEntry<S>> level3;
    private final ConcurrentHashMap<S, TranspositionTableEntry<S>> level4;
    private final ConcurrentHashMap<S, TranspositionTableEntry<S>> level5;
    //
    public TranspositionTable(int startDepth)
    {
        this.startDepth = startDepth;
        //
        level3 = new ConcurrentHashMap<>();
        level4 = new ConcurrentHashMap<>();
        level5 = new ConcurrentHashMap<>();
    }
    //
    public TranspositionTableEntry<S> getValue(int depth, S state)
    {
        int level = startDepth - depth;
        //
        switch (level)
        {

            case 3:
            case 7: return level3.get(state);
            //
            case 4:
            case 8: return level4.get(state);
            //
            case 5:
            case 9: return level5.get(state);
        }
        //
        return null;
    }
    //
    public void putValue(int depth, S state, double value, TreeNode<S> next)
    {
        int level = startDepth - depth;
        //
        switch (level)
        {
            case 3: level3.put(state, new TranspositionTableEntry<>(value, next)); break;
            case 4: level4.put(state, new TranspositionTableEntry<>(value, next)); break;
            case 5: level5.put(state, new TranspositionTableEntry<>(value, next)); break;
        }
    }
}
