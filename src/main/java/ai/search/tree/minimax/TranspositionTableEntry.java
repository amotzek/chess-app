package ai.search.tree.minimax;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
final class TranspositionTableEntry<S>
{
    private final double value;
    private final TreeNode<S> next;
    //
    public TranspositionTableEntry(double value, TreeNode<S> next)
    {
        this.value = value;
        this.next = next;
    }
    //
    public double getValue()
    {
        return value;
    }
    //
    public TreeNode<S> getNext()
    {
        return next;
    }
}
