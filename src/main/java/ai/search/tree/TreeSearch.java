package ai.search.tree;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.List;
import java.util.concurrent.ExecutionException;
/*
 * Created by andreasm on 26.05.2014.
 */
public abstract class TreeSearch<S>
{
    /**
     * Returns the origin of the search
     *
     * @return start state
     */
    protected abstract S getStart();
    /**
     * Returns the states that can be reached from the given state
     *
     * @param state given State
     * @return reachable States
     */
    public abstract S[] findSuccessors(S state);
    /**
     * Returns the value of the state, the higher the better and 1.0 denoting the destination state
     *
     * @return Value of the state
     */
    public abstract double getValue(int depth, S state);
    /**
     * Returns the best path found with the given number of tries
     *
     * @param trycount Number of tries
     * @return Path from origin to destination state
     * @throws ExecutionException - if the search is stopped
     * @throws InterruptedException - if the search is interrupted
     */
    public abstract List<S> findBestPath(long trycount) throws ExecutionException, InterruptedException;
    /**
     * Stops this search
     */
    public abstract void close();
}