/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
package ai.search.tree.montecarlo;
/*
 * Created by andreasm on 23.05.2014.
 */
public class AmbigousResultException extends IllegalStateException
{
    public AmbigousResultException(String message)
    {
        super(message);
    }
}