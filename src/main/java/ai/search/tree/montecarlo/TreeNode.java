package ai.search.tree.montecarlo;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.Arrays;
import java.util.Random;
/*
 * Created by andreasm on 22.05.2014.
 */
final class TreeNode<S>
{
    private static final double EPSILON = 1e-6;
    //
    private static final Random random = new Random();
    //
    private final MonteCarloTreeSearch<S> search;
    private final TreeNode<S> parent;
    private final int depth;
    private final S state;
    private TreeNode<S>[] children;
    private int childcount;
    private double visitcount;
    private double totalvalue;
    //
    public TreeNode(MonteCarloTreeSearch<S> search, TreeNode<S> parent, int depth, S state)
    {
        super();
        //
        this.parent = parent;
        this.search = search;
        this.depth = depth;
        this.state = state;
    }
    //
    public S getState()
    {
        return state;
    }
    //
    public double getVisitCount()
    {
        return visitcount;
    }
    //
    public double getTotalValue()
    {
        return totalvalue;
    }
    //
    public TreeNode<S> getBestChild()
    {
        if (childcount == 0) return null;
        //
        if (childcount == 1) return children[0];
        //
        var comparator = new TreeNodeComparator<S>();
        Arrays.sort(children, 0, childcount, comparator);
        var bestnode = children[0];
        double averagevisits = visitcount / childcount;
        //
        if (averagevisits * 2.0d > bestnode.visitcount) throw new AmbigousResultException(String.format("average %s, best node %s", averagevisits, bestnode.visitcount));
        //
        return bestnode;
    }
    //
    public void search()
    {
        var node = select(this);
        node = node.expand();
        double value = node.rollOut();
        propagateBack(node, value);
    }
    //
    private static <S> TreeNode<S> select(TreeNode<S> node)
    {
        TreeNode<S> lastnode;
        //
        do
        {
            lastnode = node;
            node = node.selectChild();
        }
        while (node != null);
        //
        return lastnode;
    }
    //
    private synchronized TreeNode<S> selectChild()
    {
        if (!hasChildren() || hasMoreStates()) return null;
        //
        TreeNode<S> selected = null;
        double bestbound = Double.MIN_VALUE;
        //
        for (TreeNode<S> child : children)
        {
            double bound = child.totalvalue / (child.visitcount + EPSILON)
                    + Math.sqrt(2.0d * Math.log(visitcount + 1.0d) / (child.visitcount + EPSILON))
                    + random.nextDouble() * EPSILON;
            //
            if (bound > bestbound)
            {
                selected = child;
                bestbound = bound;
            }
        }
        //
        return selected;
    }
    //
    private boolean hasMoreStates()
    {
        return children != null && childcount < children.length;
    }
    //
    private boolean hasChildren()
    {
        return childcount > 0;
    }
    //
    @SuppressWarnings("unchecked")
    private synchronized TreeNode<S> expand()
    {
        if (children == null)
        {
            var successors = search.findSuccessors(state);
            shuffle(successors);
            children = new TreeNode[successors.length];
            //
            for (int i = 0; i < successors.length; i++)
            {
                var successor = successors[i];
                children[i] = new TreeNode<>(search, this, depth - 1, successor);
            }
        }
        //
        if (childcount >= children.length) return this;
        //
        return children[childcount++];
    }
    //
    private static <S> void shuffle(S[] array)
    {
        for (int i = array.length; i > 1; i--)
        {
            int j = random.nextInt(i);
            S state = array[i - 1];
            array [i - 1] = array[j];
            array[j] = state;
        }
    }
    //
    private double rollOut()
    {
        return search.getValue(depth, state);
    }
    //
    private static <S> void propagateBack(TreeNode<S> node, double value)
    {
        while (node != null)
        {
            synchronized (node)
            {
                node.visitcount += 1.0d;
                node.totalvalue += value;
            }
            //
            node = node.parent;
        }
    }
}