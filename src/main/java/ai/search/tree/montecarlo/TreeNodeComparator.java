package ai.search.tree.montecarlo;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.Comparator;
/*
 * Created by andreasm on 23.05.2014.
 */
final class TreeNodeComparator<S> implements Comparator<TreeNode<S>>
{
    public int compare(TreeNode<S> node1, TreeNode<S> node2)
    {
        double difference = node2.getVisitCount() - node1.getVisitCount();
        //
        if (difference > 0.0d) return 1;
        //
        if (difference < 0.0d) return -1;
        //
        difference = node2.getTotalValue() - node1.getTotalValue();
        //
        return Double.compare(difference, 0.0d);
    }
}