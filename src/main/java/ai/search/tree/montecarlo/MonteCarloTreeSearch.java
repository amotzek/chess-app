package ai.search.tree.montecarlo;
/*
 * Copyright (C) 2021 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Chess App.
 *
 * You can use, redistribute and/or modify this file under the terms
 * of the Creative Commons Attribution 4.0 International Public License.
 * See https://creativecommons.org/licenses/by/4.0/ for details.
 *
 * This file is distributed in the hope that it will be useful, but
 * without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import ai.search.tree.TreeSearch;
/*
 * Created by andreasm on 22.05.2014.
 */
public abstract class MonteCarloTreeSearch<S> extends TreeSearch<S>
{
    private final ThreadPoolExecutor executor;
    //
    public MonteCarloTreeSearch(int threadcount)
    {
        executor = new ThreadPoolExecutor(0, threadcount, 60L, TimeUnit.SECONDS, new SynchronousQueue<>(), new ThreadPoolExecutor.CallerRunsPolicy());
    }
    //
    public final List<S> findBestPath(int tries) throws InterruptedException
    {
        var startstate = getStart();
        var startnode = new TreeNode<>(this, null, 0, startstate);
        var latch = new CountDownLatch(tries);
        Runnable runnable = () ->
        {
            try
            {
                startnode.search();
            }
            finally
            {
                latch.countDown();
            }
        };
        //
        while (tries-- > 0)
        {
            executor.execute(runnable);
        }
        //
        latch.await();
        var path = new LinkedList<S>();
        var node = startnode;
        //
        while (node != null)
        {
            var state = node.getState();
            path.addLast(state);
            node = node.getBestChild();
        }
        //
        return path;
    }
    //
    public final void close()
    {
        executor.shutdown();
    }
}