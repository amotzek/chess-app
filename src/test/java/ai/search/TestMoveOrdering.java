package ai.search;
//
import chess.board.Board;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
/*
 * Created by andreasm on 30.03.2022.
 */
public class TestMoveOrdering extends MoveTest
{
    @Test
    public void testSicilian()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "c7-c5", "Sg1-f3", "d7-d6", "d2-d4");
        var moves = board.generateMoves();
        //
        assertEquals("c5xd4", moves.poll().toString());
    }
    //
    @Test
    public void testAntiGruenfeld()
    {
        var board = new Board();
        board = makeMoves(board, "Sg1-f3", "Sg8-f6", "c2-c4", "g7-g6", "Sb1-c3", "d7-d5");
        var moves = board.generateMoves();
        //
        assertEquals("c4xd5", moves.poll().toString());
        assertEquals("Sc3xd5", moves.poll().toString());
        //
        board = makeMove(board, "c4xd5");
        moves = board.generateMoves();
        //
        assertEquals("Sf6xd5", moves.poll().toString());
        assertEquals("Dd8xd5", moves.poll().toString());
        //
        board = makeMoves(board, "Sf6xd5", "e2-e4");
        moves = board.generateMoves();
        //
        assertEquals("Sd5xSc3", moves.poll().toString());
        //
        board = makeMoves(board, "Sd5xSc3");
        moves = board.generateMoves();
        //
        assertEquals("b2xSc3", moves.poll().toString());
    }
}
