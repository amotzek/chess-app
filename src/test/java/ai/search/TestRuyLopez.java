package ai.search;
//
import chess.board.Board;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
/*
 * Created by andreasm on 21.10.2021.
 */
public class TestRuyLopez extends MoveTest
{
    @Test
    public void testKnight1()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "e7-e5", "Sg1-f3", "Sb8-c6", "Lf1-b5", "Lf8-c5", "c2-c3",
                "Sg8-e7", "d2-d4", "e5xd4", "c3xd4");
        board = searchMove(board, 4500000L);
        var moveString = getMoveString(board);
        assertNotEquals("Sc6xd4", moveString);
    }
    //
    @Test
    public void testKnight2()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "e7-e5", "Sg1-f3", "Sb8-c6", "Lf1-b5", "Lf8-c5", "c2-c3",
                "Sg8-e7", "d2-d4", "e5xd4", "c3xd4", "Sc6xd4");
        board = searchMove(board, 4500000L);
        var moveString = getMoveString(board);
        assertEquals("Sf3xSd4", moveString);
    }
    //
    @Test
    public void testBishop()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "e7-e5", "Sg1-f3", "Sb8-c6", "Lf1-b5", "Lf8-c5", "c2-c3",
                "Sg8-e7", "d2-d4", "e5xd4", "c3xd4", "Sc6xd4", "Sf3xSd4", "a7-a6", "Lb5-a4", "O-O", "Sd4-f5",
                "Se7xSf5", "e4xSf5");
        board = searchMove(board, 4500000L);
        var moveString = getMoveString(board);
        assertNotEquals("Lc5xf2", moveString);
    }
}
