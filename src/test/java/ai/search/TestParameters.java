package ai.search;
//
import chess.board.Board;
import chess.board.Color;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
/*
 * Created by andreasm 29.05.2022
 */
public class TestParameters extends MoveTest
{
    private static final long TRIES = 60000000L;
    //
    private static final double[] ALTERNATE_PARAMETERS = new double[] {
            8d, // Doppelbauern
            1d, // Promotion der Bauern (Endspiel)
            1d, // Zentralität der Bauern (Eröffnung)
            1d, // Zentralität der Damen und Könige (Endspiel)
            15d, // Zentralität der Springer, Läufer und Türme
            10d, // Königsangriffe
            15d, // Königsverteidigung
            80d, // Rochaden (Eröffnung)
            5000d }; // Figurenwerte
    //
    @Ignore
    @Test
    public void testParameters()
    {
        var value = playItalian() + playSicilian() + playKingsIndian() + playCatalan() + playAntiGruenfeld();
        System.out.println(value);
        //
        assertTrue(value > 0);
    }
    //
    private static int playItalian()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "e7-e5", "Sg1-f3", "Sb8-c6", "Lf1-c4", "Lf8-c5");
        //
        return playTwice(board);
    }
    //
    private static int playSicilian()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "c7-c5", "Sg1-f3", "d7-d6", "d2-d4");
        //
        return playTwice(board);
    }
    //
    private static int playKingsIndian()
    {
        var board = new Board();
        board = makeMoves(board, "d2-d4", "Sg8-f6", "c2-c4", "g7-g6", "Sb1-c3", "Lf8-g7", "e2-e4", "d7-d6");
        //
        return playTwice(board);
    }
    //
    private static int playCatalan()
    {
        var board = new Board();
        board = makeMoves(board, "d2-d4", "Sg8-f6", "c2-c4", "e7-e6", "g2-g3", "d7-d5", "Lf1-g2", "Lf8-e7", "Sg1-f3");
        //
        return playTwice(board);
    }
    //
    private static int playAntiGruenfeld()
    {
        var board = new Board();
        board = makeMoves(board, "Sg1-f3", "Sg8-f6", "c2-c4", "g7-g6", "Sb1-c3", "d7-d5");
        //
        return playTwice(board);
    }
    //
    private static int playTwice(Board board)
    {
        return play(board, Board.STANDARD_PARAMETERS, ALTERNATE_PARAMETERS) - play(board, ALTERNATE_PARAMETERS, Board.STANDARD_PARAMETERS);
    }
    //
    private static int play(Board board, double[] whiteParameters, double[] blackParameters)
    {
        while (!board.hasEnded())
        {
            var color = board.getColor();
            //
            if (color == Color.WHITE)
            {
                board = searchMove(board, TRIES, whiteParameters);
            }
            else
            {
                board = searchMove(board, TRIES, blackParameters);
            }
            //
            System.out.print(getMoveString(board));
            System.out.print(" ");
        }
        //
        Color winner = board.getWinner();
        System.out.println(winner);
        System.out.println();
        //
        if (winner == Color.WHITE) return 1;
        //
        if (winner == Color.BLACK) return -1;
        //
        return 0;
    }
}
