package ai.search;
//
import chess.board.Board;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
/*
 * Created by andreasm on 15.05.2022
 */
public class TestCastling extends MoveTest
{
    @Test
    public void testCastling()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "e7-e6", "Dd1-e2", "Sb8-c6", "g2-g3", "d7-d5", "Sb1-c3", "d5-d4",
                "Sc3-b5", "a7-a6", "Sb5-a3", "Sg8-f6", "Sg1-f3", "Lf8-c5", "e4-e5", "Sf6-d5", "c2-c4", "d4xc3 e.p.",
                "b2xc3", "b7-b5", "Sa3-c2");
        board = searchMove(board, 60000000L);
        var moveString = getMoveString(board);
        assertEquals("O-O", moveString);
    }
}
