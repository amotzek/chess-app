package ai.search;
//
import chess.board.Board;
import org.junit.Test;
import static org.junit.Assert.assertNotEquals;
/*
 * Created by andreasm on 11.07.2022
 */
public class TestBlunder extends MoveTest
{
    @Test
    public void testPromotion()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "c7-c5", "Sg1-f3", "Sb8-c6", "d2-d4", "c5xd4", "Sf3xd4", "Sg8-f6",
                "Sb1-c3", "Dd8-a5", "Sd4-b3", "Da5-e5", "f2-f4", "De5-d6", "e4-e5", "Dd6xDd1", "Sc3xDd1", "Sf6-d5",
                "c2-c4", "Sd5-b4", "Sd1-e3", "g7-g5", "f4xg5", "Sc6xe5", "Lc1-d2", "Sb4-d3", "Lf1xSd3", "Se5xLd3",
                "Ke1-e2", "Sd3-f4", "Ke2-f3", "Sf4-e6", "h2-h4", "h7-h6", "Ld2-c3", "Th8-g8", "Ta1-d1", "h6xg5",
                "h4-h5", "b7-b6", "h5-h6");
        board = searchMove(board, 60000000L);
        var moveString = getMoveString(board);
        assertNotEquals("Tg8-g6", moveString);
    }
}
