package ai.search;
//
import chess.board.Board;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
/*
 * Created by andreasm on 25.09.2021
 */
public class TestFoolsMate extends MoveTest
{
    @Test
    public void testFoolsMate()
    {
        var board = new Board();
        board = makeMoves(board, "f2-f3", "e7-e6", "g2-g4");
        board = searchMove(board, 90000000L);
        var moveString = getMoveString(board);
        assertEquals("Dd8-h4", moveString);
    }
    //
    @Test
    public void testTeedDelmar()
    {
        var board = new Board();
        board = makeMoves(board, "d2-d4", "f7-f5", "Lc1-g5", "h7-h6", "Lg5-h4", "g7-g5", "Lh4-g3");
        board = searchMove(board, 30000000L);
        var moveString = getMoveString(board);
        assertNotEquals("f5-f4", moveString);
    }
}
