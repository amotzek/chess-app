package ai.search;
//
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
/*
 * Created by andreasm on 22.02.2020.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({TestBoard.class, TestMoveGenerator.class, TestMoveOrdering.class,
        TestFoolsMate.class, TestBlunder.class, TestRuyLopez.class, TestCastling.class, TestDepth.class})
public class AiTests
{
}