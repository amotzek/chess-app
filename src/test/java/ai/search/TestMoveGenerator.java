package ai.search;
//
import chess.board.Board;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
/*
 * Created by andreasm on 04.10.2021.
 */
public class TestMoveGenerator extends MoveTest
{
    @Test
    public void testKasparowTopalow()
    {
        var board = new Board();
        board = makeMoves(board, "e2-e4", "d7-d6", "d2-d4", "Sg8-f6", "Sb1-c3", "g7-g6",
                "Lc1-e3", "Lf8-g7", "Dd1-d2", "c7-c6", "f2-f3", "b7-b5", "Sg1-e2", "Sb8-d7",
                "Le3-h6", "Lg7xLh6", "Dd2xLh6", "Lc8-b7", "a2-a3", "e7-e5", "O-O-O", "Dd8-e7",
                "Kc1-b1", "a7-a6", "Se2-c1", "O-O-O", "Sc1-b3", "e5xd4", "Td1xd4", "c6-c5",
                "Td4-d1", "Sd7-b6", "g2-g3", "Kc8-b8", "Sb3-a5", "Lb7-a8", "Lf1-h3", "d6-d5",
                "Dh6-f4", "Kb8-a7", "Th1-e1", "d5-d4", "Sc3-d5", "Sb6xSd5", "e4xSd5", "De7-d6");
        //
        assertEquals("La8", board.getPieceAt(0, 7).toString());
        assertEquals("Td8", board.getPieceAt(3, 7).toString());
        assertEquals("Th8", board.getPieceAt(7, 7).toString());
        assertEquals("Ka7", board.getPieceAt(0, 6).toString());
        assertEquals("f7", board.getPieceAt(5, 6).toString());
        assertEquals("h7", board.getPieceAt(7, 6).toString());
        assertEquals("Dd6", board.getPieceAt(3, 5).toString());
        assertEquals("Df4", board.getPieceAt(5, 3).toString());
        assertEquals("Lh3", board.getPieceAt(7, 2).toString());
        assertEquals("Kb1", board.getPieceAt(1, 0).toString());
        //
        board = makeMoves(board, "Td1xd4", "c5xTd4", "Te1-e7", "Ka7-b6", "Df4xd4",
                "Kb6xSa5", "b2-b4", "Ka5-a4", "Dd4-c3", "Dd6xd5", "Te7-a7", "La8-b7", "Ta7xLb7", "Dd5-c4",
                "Dc3xSf6", "Ka4xa3", "Df6xa6", "Ka3xb4", "c2-c3", "Kb4xc3", "Da6-a1", "Kc3-d2",
                "Da1-b2", "Kd2-d1");
        //
        assertEquals("Td8", board.getPieceAt(3, 7).toString());
        assertEquals("Th8", board.getPieceAt(7, 7).toString());
        assertEquals("Dc4", board.getPieceAt(2, 3).toString());
        assertEquals("Kd1", board.getPieceAt(3, 0).toString());
        assertEquals("Kb1", board.getPieceAt(1, 0).toString());
        assertEquals("Db2", board.getPieceAt(1, 1).toString());
        assertEquals("Lh3", board.getPieceAt(7, 2).toString());
        //
        var builder = new StringBuilder();
        //
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                var piece = board.getPieceAt(x, y);
                //
                if (piece == null) continue;
                //
                var position = piece.getPosition();
                //
                builder.append(position);
                builder.append(" ");
            }
        }
        //
        assertEquals("b1 b2 b5 b7 c4 d1 d8 f3 f7 g3 g6 h2 h3 h7 h8 ", builder.toString());
    }
}
