package ai.search;
//
import chess.board.Board;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
/*
 * Created by andreasm on 03.11.2021.
 */
public class TestDepth extends MoveTest
{
    @Test
    public void testSlavDefense()
    {
        var board = new Board();
        board = makeMoves(board, "d2-d4", "d7-d5", "c2-c4", "c7-c6", "Sg1-f3", "Sg8-f6", "Sb1-c3",
                "d5xc4", "a2-a4");
        board = searchMove(board, 60000000L);
        int pathLength = 0;
        //
        while (board != null)
        {
            board = board.getNextBoard();
            pathLength++;
        }
        //
        assertTrue(pathLength > 3);
    }
    //
    @Test
    public void testEnglish()
    {
        var board = new Board();
        board = makeMoves(board, "c2-c4", "c7-c5", "Sg1-f3", "Sg8-f6", "g2-g3", "Sb8-c6", "Sb1-c3",
                "e7-e5", "e2-e4", "a7-a5", "d2-d3");
        board = searchMove(board, 60000000L);
        int pathLength = 0;
        //
        while (board != null)
        {
            board = board.getNextBoard();
            pathLength++;
        }
        //
        assertTrue(pathLength > 3);
    }
}
