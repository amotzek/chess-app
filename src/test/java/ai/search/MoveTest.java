package ai.search;
//
import chess.board.Board;
import chess.search.ChessMiniMaxSearch;
/*
 * Created by andreasm 25.09.2021
 */
public abstract class MoveTest
{
    protected static Board makeMove(Board board, String moveString)
    {
        var queue = board.generateMoves();
        //
        while (!queue.isEmpty())
        {
            var move = queue.poll();
            //
            if (moveString.equals(move.toString())) return board.applyMove(move);
        }
        //
        throw new IllegalArgumentException(moveString + " is not a valid move");
    }
    //
    protected static Board makeMoves(Board board, String... moveStrings)
    {
        for (var moveString : moveStrings)
        {
            board = makeMove(board, moveString);
        }
        //
        return board;
    }
    //
    protected static Board searchMove(Board board, long tries, double[] parameters)
    {
        var search = new ChessMiniMaxSearch(board, parameters);
        var path= search.findBestPath(tries);
        //
        if (path.size() > 1) return path.get(1);
        //
        throw new IllegalStateException("cannot find move");
    }
    //
    protected static Board searchMove(Board board, long tries)
    {
        return searchMove(board, tries, Board.STANDARD_PARAMETERS);
    }
    //
    protected static String getMoveString(Board board)
    {
        return board.getBeforeMove().toString();
    }
}
