package ai.search;
//
import java.util.TreeSet;
import chess.board.Board;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
/*
 * Created by andreasm on 27.05.2014.
 */
public class TestBoard
{
    @Test
    public void testInitialBoard()
    {
        var board = new Board();
        var moves = new TreeSet<String>();
        var queue = board.generateMoves();
        //
        while (!queue.isEmpty())
        {
            var move = queue.poll();
            moves.add(move.toString());
        }
        //
        assertEquals("Sb1-a3", moves.pollFirst());
        assertEquals("Sb1-c3", moves.pollFirst());
        assertEquals("Sg1-f3", moves.pollFirst());
        assertEquals("Sg1-h3", moves.pollFirst());
        assertEquals("a2-a3", moves.pollFirst());
        assertEquals("a2-a4", moves.pollFirst());
        assertEquals("b2-b3", moves.pollFirst());
        assertEquals("b2-b4", moves.pollFirst());
        assertEquals("c2-c3", moves.pollFirst());
        assertEquals("c2-c4", moves.pollFirst());
        assertEquals("d2-d3", moves.pollFirst());
        assertEquals("d2-d4", moves.pollFirst());
        assertEquals("e2-e3", moves.pollFirst());
        assertEquals("e2-e4", moves.pollFirst());
        assertEquals("f2-f3", moves.pollFirst());
        assertEquals("f2-f4", moves.pollFirst());
        assertEquals("g2-g3", moves.pollFirst());
        assertEquals("g2-g4", moves.pollFirst());
        assertEquals("h2-h3", moves.pollFirst());
        assertEquals("h2-h4", moves.pollFirst());
        assertTrue(moves.isEmpty());
    }
    //
    @Test
    public void testHashCode()
    {
        var board = new Board();
        //
        assertEquals(-574197748 + 3, board.hashCode());
    }
}