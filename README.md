# Schach #

![Screenshot](russische-verteidigung.png)

Die Datei chess.jar aus den Downloads enthält ein Schachprogramm. 
Das Programm kann durch einen Doppelklick auf die Datei oder mit

java -jar chess.jar

gestartet werden. Der menschliche Spieler zieht die weißen Figuren durch Klicken auf das Ausgangs- und das Zielfeld. 
Danach errechnet das Programm einen Zug für die schwarzen Figuren.

Das Programm besteht im wesentlichen aus vier Modulen:

- Repräsentation - Darstellen von Spielsituationen,
- Zuggenerator - Bestimmen von möglichen Zügen,
- Strategie - Auswählen des besten Zugs,
- User Interface - Schnittstelle zum menschlichen Spieler.


## Repräsentation ##

Die beiden Instanzen der Enumeration Color repräsentieren die am Spiel beteiligten Farben weiss und schwarz.

Instanzen der Klasse Position repräsentieren die Positionen auf dem Schachbrett (z.B. e2).

Instanzen der Klasse Piece repräsentieren die Figuren und ihre Positionen während des Spiels. 
Die verschiedenen Figurenarten sind durch Subklassen - King, Queen, Rook, Bishop, Knight und Pawn - realisiert. 
Jede Figur vermerkt die Historie ihrer Bewegungen.

Jede Instanz der Klasse Board verwaltet eine Stellung auf dem Schachbrett. 
Es werden alle beteiligten Figuren (auch die geschlagenen), die Farbe am Zug sowie ein Verweis auf die vorangegangene Stellung gespeichert.


## Zuggenerator ##

Die Klasse Move repräsentiert Halbzüge. Sie hat Subklassen für spezielle Arten von Zügen, wie die Rochade oder die Umwandlung. 
Ein Halbzug wird durch Verweise auf bis zu zwei Instanzen der Klasse Piece beschrieben. 
Ein gewöhnlicher Halbzug wie z.B. e2-e4 erfordert nur einen Verweis auf eine Figur, den Verweis auf einen Bauern in Position e4. 
Die vorherige Position des Bauern wird vom Figuren-Objekt selbst gespeichert. 
Eine Rochade oder das Schlagen einer Figur erfordert das Vermerken von zwei Verweisen auf die beiden beteiligten Figuren.

Die Klasse Board definiert eine Methode zum Generieren aller möglichen Halbzüge. 
Sie greift auf Figuren-spezifische Methoden zurück. 
Die Klassen für die Figuren haben dafür neben Methoden zum Abfragen der elementaren Eigenschaften wie Farbe, Position und Art der Figur 
eine Methode, die abhängig von den Positionen der anderen Figuren auf dem Schachbrett alle mit der Figur möglichen Halbzüge 
in eine Warteschlange einfügt.


## Strategie ##

Die Klasse OrderedMoves implementiert zusammen mit der Klasse MoveComparator eine Prioritätswarteschlange für Halbzüge. 
Ein Halbzug wird vor einem anderen Halbzug in die Schlange eingefügt, wenn

- er der beste Zug für die Stellung aus der vorangegangenen Suche ist,
- er eine Umwandlung ist und der andere nicht,
- bei ihm eine Figur geschlagen wird und beim anderen nicht,
- bei ihm eine höherwertige Figur geschlagen wird als beim anderen,
- der Zug auf ein Feld führt, das besser durch andere Figuren verteidigt wird als beim anderen Zug.

Die Kriterien werden in der obigen Reihenfolge überprüft. Wenn die Halbzüge aus der Schlange entnommen werden, 
um im Mini-Max-Algorithmus verarbeitet zu werden, ergibt sich aufgrund der Sortierung ein kleinerer Suchbaum.

Die statische Bewertung von Stellungen erfolgt durch eine Methode der Klasse Board. Die Bewertung ist umso höher

- je mehr Figuren noch im Spiel sind,
- je näher die Figuren dem gegnerischen König sind,
- je näher die Offiziere der Brettmitte sind und
- je näher die Bauern der gegnerischen Grundreihe sind.

Der Mini-Max-Algorithmus selbst findet sich in der Klassen ChessMiniMaxSearch und ihrer Superklasse. 
Der Algorithmus benutzt Alpha-Beta-Pruning und eine Transposition Table.


## User Interface ##

Der Code für die Benutzeroberfläche ist nach dem Design Pattern Model View Controller strukturiert. 
Es gibt Modelle für das Spiel, das Brett und die möglichen Züge. Spezialisierte View-Klassen existieren für das Hauptfenster, 
den darin enthaltenen Bereich zur Darstellung des Schachbretts und ein Popup-Menü zur Auswahl aus mehreren Zugmöglichkeiten bei einer Umwandlung. 
Für jede dieser Klassen existiert eine spezialisierte Controller-Klasse.